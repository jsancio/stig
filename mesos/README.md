# Stig on Mesos
For my hack project and newbie framework here is an implementation of Stig on Mesos.

## What is stig?
Stig is a Scala framework for executing workflows in a scalable, debugabble and
asynchronous manner. Up to this point the only implementation relied on AWS' SWF
to manage state and for scheduling. It was always the goal for Stig to provide other
implementations. Mesos is a great platform to use as the base of such an implementation.

Both SWF and Mesos are great frameworks/services for writing distributed applications.
Unfortunately their APIs are too low level. SWF's API is an event stream where each
action is represented as an event in this stream. Mesos's has event driven API using
callbacks. They both lead to code that is very difficult to read. Wouldn't it be nice
to write asynchronous as if it was sequential code?

Stig is useful for writing short lived workflows like video encoding or virtual machine
provitioning and configuration. Stig is also useful for writing long lived workflows
like data migration and order fullfilment.

## How does it work?
At a high-level you have two type of Actors: Deciders and Tasks. Deciders are
resposible for determining what to execute based on the history of a workflow. Tasks
are asynchronous units of work. Task accept an input and generate a result. Tasks are
also allowed to have side-effects. Deciders accepts an input, can create other Tasks
(there are few other mechanisms supported but this hack project only supports Tasks) and
cannot generate any side-effect.

### Decider
The best way to understand how deciders work is to see an example. Through this
document we are going to try sum the square root of a list of numbers. In Scala you
would compute this as follow:

```
scala> val numbers = (1 to 100).map(_.toDouble)
numbers: IndexedSeq[Double] = ...
scala> numbers.map(Math.sqrt(_)).sum
res0: Double = 671.4629471031477
```

It is helpful to have this to see how it relates to Stig. This solution is actually
cheating a bit as Scala has a 'magical' function that sums the elements of a collection.
A more generic solution looks as follow:

```
scala> numbers.map(Math.sqrt(_)).fold(0d)(_ + _)
res5: Double = 671.4629471031477
```

This code is sequential. What if we wanted to compute the square roots in parallel
accross your datacenter and then sum the result of such a computation? How would we do
this in Stig?

```
def handleDecision(context: DeciderService, input: Array[Double]): Unit = {
  ...

  for {
    squareRoots <- Later.sequence(input.map(SquareRootActivity.schedule(_)))
    sum <- SumActivity.schedule(squareRoots)
  } {
    context.completeWorkflow(sum.toString)
  }
}
```

The variable `input` in the Stig implementation maps to `(1 to 100).map(.toDouble)` in
the sequential solution. `squareRoots <- ...` maps to `Math.sqrt(_)`, and `sum <- ...`
maps `fold(0d)(_ + _)`. Let's look at the expressions in the Stig implementation as they
get evaluated.

1. `SquareRootActivity.schedule(_)`. This creates a Mesos' Task for each number in
`input` and finds the square root. The type returned by this expression is
`Later[Double]`. You can think of Stig's `Later` as the same as Scala's `Future` and
libprocess' `Future`.

2. `Later.sequence(...)`. This waits for all of the square roots (`Later`s) to complete
by turning an `Array[Later[Double]]` into a `Later[Array[Double]]`.

3. `SumActivity.schedule(...)`. Sends the `Array[Double]` (the squareRoots) to another
Task and returns the sum as a `Later[Double]`.

4. Finally `context.completeWorkflow(...)` sends the result to the Actor that started
the workflow.

At a high-level this workflow is computing the square root of each element in the array
in parallel and sends the result of those parallel executions to another Task to sum
them.

### Task
The implementation for Tasks is even simpler. The task that computes the square root
looks as follow:

```
def handleActivity(context: ActivityService, input: Double): Double = {
  Math.sqrt(input)
}
```

The task that computes the sum looks as follow:

```
def handleActivity(context: ActivityService, input: Array[Double]): Double = {
  input.sum
}
```

The execution of Stig's Task in the Mesos implementation works by starting a Mesos
Executor that knows how to call the appropriate closure based on the Mesos task.

### Demo
We can execute this demo as follow:

```
sbt "mesos/runMain stig.mesos.Main"
...
[info] We can launch 20 tasks
[info] task queue is empty
[info] We can launch 20 tasks
[info] launching 20
[info] We can launch 20 tasks
[info] launching 20
[info] We can launch 20 tasks
[info] launching 20
[info] We can launch 20 tasks
[info] launching 20
[info] We can launch 20 tasks
[info] launching 20
[info] We can launch 20 tasks
[info] launching 1
[info] ----------------------------------------------------------------------
[info] Result: 671.4629471031477
[info] ----------------------------------------------------------------------
```

Launching only 20 tasks at a time is an artifical limitation due to how I hacked the
implementation and my Mesos deployment for the demo. In a future implementation we
should be able to launch all 100 tasks concurrently.

## Future Work
There are many features in Stig that are not supported in this Mesos implementation.
Some of those are:

1. Task failure
2. Task timeout
3. Timers
4. Throttling
5. Persistent operations (task) log
6. HA
7. Placement strategy (maps to Mesos evaluating a Mesos offer or task list in SWF).
8. And many more

## The code
You can checkout the code at https://bitbucket.org/jsancio/stig. It contains three
subprojects. `core` has all of the implementation agnostic functionality. `aws`
contains the implementation for AWS' SWF. `mesos` contains the hacked implementation
for Mesos.
