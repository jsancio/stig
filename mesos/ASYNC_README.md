# Asynchronous programming with Scala and Mesos

The dictionary defines asynchronous as operations without the use of a fixed time intervals. The
most common example of asynchronous code is UI programming where UI events are put in an event
queue and the UI programent event loop is responsible for picking up UI events from the queue and
process them with the appropriate handler.

Mesos' API works in a similar manner instead of UI events like mouse clicks and keyboard presses we
have [Offers](https://github.com/apache/mesos/blob/master/include/mesos/mesos.proto#L597-L609),
[TaskStatus](https://github.com/apache/mesos/blob/master/include/mesos/mesos.proto#L662-L708), etc.

The major complexity of asynchronous programming that on a single threaded implementation the order
evaluation/execution is dependent on the order events in the event queue. In the case of UI events
and Mesos events that order is not deterministic.

For the rest of this document we are going to use an example to illustrate the difference bewteen
the two programming models. Let's assume that we want process images uploaded to a service by
resizing them and/or applying filters targeting a set of platforms (E.g. phone, tablet and
desktop). The implementation that we will consider will download the image from a remote URI to
memory, process the image in memory and store it processed result to a remote URI.

## Sequential and Blocking

A sequential and blocking implementation in Scala of the problem above looks as follow (removed
some of the details for clarity):

```scala
def processImage(image: URI, platforms: Set[Platform]): Set[URI] = {
  val data = downloadImage(image)

  platforms.map { platform =>
    uploadImage(
      processImage(data, platform),
      generateUri(image, platform))
  }
}

def downloadImage(image: URI): Array[Byte] = {
  // Long operation that download an image and stores it in local storage
  ... 
}

def processImage(image: Array[Byte], platform: Platform): Array[Byte] = {
  // Converts the image and uploads it to the return URI
  ...
}

def uploadImage(image: Array[Byte], uri: URI): URI = {
  // Upload the image to remote URI
  ...
}

def generateUri(image: URI, platform: Platform): URI = {
  // Generate an URI given the original image URI and the platform.
  ...
}
```

In general the code is execute order of evaluate defined by Scala. In this case we first download
the image to memory. For every platform passed into the function we process the image to the
platform, generate a platform specific URI and finally upload the processed image to the generated
URI.

All of them function called in this implementation are blocking. In the case of `downloadImage` and
`uploadImage` the thread maybe be block for milliseconds waiting for the image to download and
upload respectively. The problem with processImage is little different. Because the code is
executed sequentially this solution will only utilize one CPU (core) even though we may have
multiple CPUs available to the process.

We can improve this code by removing all of the blocking operations/functions and replacing them
with asynchronous execution and non-blocking operations.

## Asynchronous Programming with Scala

The idiomatic way of writing asynchronous non-blocking code in Scala looks as follow:

```scala
def processImage(image: URI, platforms: Set[Platform])(
  implicit context: ExecutionContext): Future[Set[URI]] = {

  for {
    data <- downloadImage(image)
    images <- Future.traverse(platforms) { platform =>
      for {
        platformData <- processImage(data, platform)
        platformUri <- uploadImage(platformData, generateUri(image, platform))
      } yield platformUri
    }
  } yield images
}

def downloadImage(image: URI)(
  implicit context: ExecutionContext): Future[Array[Byte]] = {
  // Long operation that download an image and stores it in local storage
  ...
}

def processImage(image: Array[Byte], platform: Platform)(
  implicit context: ExecutionContext): Future[Array[Byte]] = {
  // Converts the image and uploads it to the return URI
  ...
}

def uploadImage(image: Array[Byte], uri: URI)(
  implicit context: ExecutionContext): Future[URI] = {
  // Upload the image to remote URI
  ...
}
```

We can refactor all of the blocking methods to return a promise for a value instead of the actual
value. In Scala we do this by returning a `Future[T]`. For those not familiar with Scala's standard
library, a [Future](http://docs.scala-lang.org/overviews/core/futures.html) is an object holding a
value which may become available at some point.

Scala's `Future` implement methods like `flatMap`, `map`, `filterWith` and `foreach` which allow
it to integrate with Scala `for-comprehension` syntax. The nested for-comprehensions above
translate to:

```scala
downloadImage(image).flatMap { data =>
  Future.traverse(platforms) { platform =>
    processImage(data, platform).flatMap { platformData =>
      uploadImage(platformData, generateUri(image, platform)).map { platformUri =>
        platformUri
      }
    }
  } map { images => images }
}
```

Each closure passed to `flatMap` and `map` (and the closure passed to `Future.traverse`) gets
converted to a Java `Runnable` that gets executed in the associated ExecutionContext when the
future completes. Given the asynchronoush nature of the mechanism the order of execution is
non-deterministic. The key take away here is that easy to read and sequentially looking code was
translated to asynchronous (and possibly parallel) non-blocking callback based code.

This is a very powerfull improvement but maybe we can do better. The algoritm executed in
`processImage` is most likely CPU bound yet the code outline above can only execute on one
computer. This means that we can only process as many images in parallel as we have CPUs. We can
leverage Mesos to execute the image processing accross all the CPUs and memory in our datacenter.

## Asynchronous Programming with Scala and Mesos

For this solution we are going to change the code a bit. To avoid having to send the image to all
of the server processing images we are going to first download the image to a low latency "local"
storage accessable to all of our servers. Second, we are going to process the image for all of the
provided platforms. Lastly we are going to delete the "locally" stored original image.

This means that we have three type of Mesos Tasks:

1. LocalDownloadTask - This task downloads the remote image to a "local" storage that provided low
latency access to all of the servers in our datacenter.

1. ProcessImageTask - This task is responsible for downloading the image from the "local" storage
to memory and processing the image the specificed platform. We execute this once for every platform
times every image.

1. LocalDeleteTask - This task execute once all of the above task have completed and is responsible
for deleting the locally stored image.

We can see the high-level implmentation in the code below. Full implementation was omited in the
interest of brevety:

```scala
def processImage(image: URI, platforms: Set[Platform])(
  implicit context: DeciderService): Later[Set[URI]] = {

  for {
    localUri <- LocalDownloadTask.schedule(image)
    images <- Later.traverse(platforms) { platform =>
      ProcessImageTask.schedule(localUri, platform)
    }
    _ <- LocalDeleteTask.schedule(localUri)
  } yield images
}

object ProcessImageTask {
  def schedule(image: URI, platform: Platform)(implicit context: DeciderService): Later[URI] = {
    // Schedules the process image task with Mesos
    ...
  }
}

def processImageTask(image: URI, platform: Platform)(
  implicit context: ExecutionContext): Future[URI] = {

  for {
    data <- downloadImage(image)
    platformData <- processImage(data, platform)
    platformUri <- uploadImage(platformData, generateUri(image, platform))
  } yield platformUri
}
```

Mesos an API for communicate with the
[Mesos Master](https://mesos.apache.org/api/latest/java/org/apache/mesos/Scheduler.html) and a
second API for communicating with the
[Mesos Slave](https://mesos.apache.org/api/latest/java/org/apache/mesos/Executor.html). Both API
provide an asynchronous non-blocking callback based API. With the help of the Scala language we are
able to provide an imperative looking API.

### How does it work?

At a high-level you have two type of Actors: Deciders and Tasks. Deciders are
resposible for determining what to execute based on the history of a workflow. Tasks
are asynchronous units of work. Task accept an input and generate a result. Tasks are
also allowed to have side-effects. Deciders accepts an input, can create other Tasks
(there are few other mechanisms supported but this hack project only supports Tasks) and
cannot generate any side-effect. The execution model is such that Decider executes on the process
registered as the Mesos Scheduler and Task execute on the process registered as the Mesos Executor.

In the example above `processImage` method gets executed as
the Decider in the process executing as a Mesos Framework Scheduler.
`LocalDownloadTask.schedule(...)` schedules a task with the Mesos Master to run on a Mesos Slave.
Once that task completes, `ProcessImageTask.schedule(...)` is used to schedule a process image
task in parallel for every supported platform. Once all of the tasks complete, the task responsible
for deleting the locally downloaded image is scheduled with `LocalDeleteTask.schedule(...)`.

## Conclusion

There are many features that are not supported in this basic implementation using Mesos. Some of
those are:

1. Task failure
1. Task timeout
1. Timers
1. Throttling
1. Persistent operations (task) log
1. HA
1. Placement strategy (maps to Mesos evaluating a Mesos offer or task list in SWF).
1. And many more

