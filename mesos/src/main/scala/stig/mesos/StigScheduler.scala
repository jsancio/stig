package stig.mesos

import scala.collection.JavaConverters.asScalaBufferConverter

import akka.actor.ActorRef
import org.apache.mesos.Protos
import org.apache.mesos.Scheduler
import org.apache.mesos.SchedulerDriver

import stig.mesos.model.Offer
import stig.mesos.model.TaskStatus

final class StigScheduler(scheduler: ActorRef) extends Scheduler {
  def error(driver: SchedulerDriver, message: String): Unit = {}

  def executorLost(
    driver: SchedulerDriver,
    executorId: Protos.ExecutorID,
    slaveId: Protos.SlaveID,
    status: Int): Unit = {}

  def slaveLost(driver: SchedulerDriver, slaveId: Protos.SlaveID): Unit = {}

  def disconnected(driver: SchedulerDriver): Unit = {}

  def frameworkMessage(
    driver: SchedulerDriver,
    executorId: Protos.ExecutorID,
    slaveId: Protos.SlaveID,
    data: Array[Byte]): Unit = {}

  def statusUpdate(driver: SchedulerDriver, status: Protos.TaskStatus): Unit = {
    scheduler ! TaskStatus(driver, status)
  }

  def offerRescinded(driver: SchedulerDriver, offerId: Protos.OfferID): Unit = {}

  def resourceOffers(driver: SchedulerDriver, offers: java.util.List[Protos.Offer]): Unit = {
    // We are just going to pass the offers to the scheduling actor
    offers.asScala.foreach { offer =>
      scheduler ! Offer(driver, offer)
    }
  }

  def reregistered(driver: SchedulerDriver, masterInfo: Protos.MasterInfo): Unit = {}

  def registered(
    driver: SchedulerDriver,
    frameworkId: Protos.FrameworkID,
    masterInfo: Protos.MasterInfo): Unit = {}
}
