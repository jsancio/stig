package stig.mesos.demo

import java.net.URI

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import stig.util.Later
import stig.DeciderService

object Examples extends App {
  // Sequential
  def processImage(image: URI, platforms: Set[Platform]): Set[URI] = {
    val data = downloadImage(image)

    platforms.map { platform =>
      uploadImage(
        processImage(data, platform),
        generateUri(image, platform))
    }
  }

  def downloadImage(image: URI): Array[Byte] = {
    // Long operation that download an image and stores it in local storage
    ???
  }

  def processImage(image: Array[Byte], platform: Platform): Array[Byte] = {
    // Converts the image and uploads it to the return URI
    ???
  }

  def uploadImage(image: Array[Byte], uri: URI): URI = {
    // Upload the image to remote URI
    ???
  }

  def generateUri(image: URI, platform: Platform): URI = {
    ???
  }
}

object AsyncExamples extends App {
  def processImage(image: URI, platforms: Set[Platform])(
    implicit context: ExecutionContext): Future[Set[URI]] = {

    for {
      data <- downloadImage(image)
      images <- Future.traverse(platforms) { platform =>
        for {
          platformData <- processImage(data, platform)
          platformUri <- uploadImage(platformData, generateUri(image, platform))
        } yield platformUri
      }
    } yield images
  }

  def uglyProcessImage(image: URI, platforms: Set[Platform])(
    implicit context: ExecutionContext): Future[Set[URI]] = {

    downloadImage(image).flatMap { data =>
      Future.traverse(platforms) { platform =>
        processImage(data, platform).flatMap { platformData =>
          uploadImage(platformData, generateUri(image, platform)).map { platformUri =>
            platformUri
          }
        }
      } map { images => images }
    }
  }

  def downloadImage(image: URI)(
    implicit context: ExecutionContext): Future[Array[Byte]] = {
    // Long operation that download an image and stores it in local storage
    ???
  }

  def processImage(image: Array[Byte], platform: Platform)(
    implicit context: ExecutionContext): Future[Array[Byte]] = {
    // Converts the image and uploads it to the return URI
    ???
  }

  def uploadImage(image: Array[Byte], uri: URI)(
    implicit context: ExecutionContext): Future[URI] = {
    // Upload the image to remote URI
    ???
  }

  def generateUri(image: URI, platform: Platform): URI = {
    ???
  }
}

object MesosAsyncExamples extends App {
  def processImage(image: URI, platforms: Set[Platform])(
    implicit context: DeciderService): Later[Set[URI]] = {

    for {
      localUri <- scheduleLocalDownloadTask(image)
      images <- Later.traverse(platforms) { platform =>
        scheduleProcessImageTask(localUri, platform)
      }
      _ <- scheduleLocalDeleteTask(localUri)
    } yield images
  }

  def scheduleLocalDownloadTask(image: URI)(
    implicit context: DeciderService): Later[URI] = {

    ???
  }

  def scheduleLocalDeleteTask(localUri: URI)(
    implicit context: DeciderService): Later[Unit] = {

    ???
  }

  def scheduleProcessImageTask(image: URI, platform: Platform)(
    implicit context: DeciderService): Later[URI] = {

    ???
  }

  def processImageTask(image: URI, platform: Platform)(
    implicit context: ExecutionContext): Future[URI] = {
    for {
      data <- downloadImage(image)
      platformData <- processImage(data, platform)
      platformUri <- uploadImage(platformData, generateUri(image, platform))
    } yield platformUri
  }

  def downloadImage(image: URI)(
    implicit context: ExecutionContext): Future[Array[Byte]] = {
    // Long operation that download an image and stores it in local storage
    ???
  }

  def processImage(image: Array[Byte], platform: Platform)(
    implicit context: ExecutionContext): Future[Array[Byte]] = {
    // Converts the image and uploads it to the return URI
    ???
  }

  def uploadImage(image: Array[Byte], uri: URI)(
    implicit context: ExecutionContext): Future[URI] = {
    // Upload the image to remote URI
    ???
  }

  def generateUri(image: URI, platform: Platform): URI = {
    ???
  }
}

case class Platform()
