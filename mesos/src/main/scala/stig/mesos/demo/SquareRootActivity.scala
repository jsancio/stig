package stig.mesos.demo

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

import org.joda.time.Seconds

import stig.ActivityHandler
import stig.ActivityService
import stig.DeciderService
import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.TaskList
import stig.serial.Reader
import stig.serial.Writer
import stig.util.Later

object SquareRootActivity {
  val activity = Activity("demo-square-root", "1.0")

  def schedule(value: Double)(implicit context: DeciderService): Later[Double] = {
    implicit val inputWriter = new DoubleWriter {}
    implicit val resultReader = new DoubleReader {}

    context.scheduleActivity(activity, TaskList("ignore"), timeout, value)
  }

  private[this] val timeout = ActivityTimeout(
    Seconds.seconds(1),
    Seconds.seconds(1),
    Seconds.seconds(1),
    Seconds.seconds(1))
}

final class SquareRootActivity extends ActivityHandler[Double, Double] with DoubleReader with DoubleWriter {
  override def handleActivity(context: ActivityService, input: Double): Double = {
    Math.sqrt(input)
  }
}

trait DoubleWriter extends Writer[Double] {
  def write(output: Double): Array[Byte] = {
    val buffer = new ByteArrayOutputStream()
    val serializer = new ObjectOutputStream(buffer)
    serializer.writeDouble(output)
    serializer.close()

    buffer.toByteArray
  }
}

trait DoubleReader extends Reader[Double] {
  def read(input: Array[Byte]): Double = {
    val deserializer = new ObjectInputStream(new ByteArrayInputStream(input))
    deserializer.readDouble()
  }
}
