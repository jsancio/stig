package stig.mesos.demo

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

import stig.DeciderService
import stig.DecisionHandler
import stig.serial.Reader
import stig.serial.Writer
import stig.util.Later

final class DemoDecider extends DecisionHandler[Seq[Double]] with SeqDoubleReader {
  override def handleDecision(context: DeciderService, input: Seq[Double]): Unit = {
    implicit val implicitContext = context

    /* TODO: change this so that instead:
     * 1. we download a set of images
     * 2. resize it to three different sizes
     * 3. store the URIs on a database
     */

    for {
      squareRoots <- Later.sequence(input.map(SquareRootActivity.schedule(_)))
      sum <- SumActivity.schedule(squareRoots)
    } {
      context.completeWorkflow(sum.toString)
    }
  }
}

trait SeqDoubleReader extends Reader[Seq[Double]] {
  def read(input: Array[Byte]): Seq[Double] = {
    val deserializer = new ObjectInputStream(new ByteArrayInputStream(input))
    deserializer.readObject().asInstanceOf[Seq[Double]]
  }
}

trait SeqDoubleWriter extends Writer[Seq[Double]] {
  def write(output: Seq[Double]): Array[Byte] = {
    val buffer = new ByteArrayOutputStream()
    val serializer = new ObjectOutputStream(buffer)
    serializer.writeObject(output)
    serializer.close()

    buffer.toByteArray
  }
}
