package stig.mesos.demo

import org.joda.time.Seconds

import stig.ActivityHandler
import stig.ActivityService
import stig.DeciderService
import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.TaskList
import stig.util.Later

object SumActivity {
  val activity = Activity("demo-sum", "1.0")

  def schedule(value: Seq[Double])(implicit context: DeciderService): Later[Double] = {
    implicit val inputWriter = new SeqDoubleWriter {}
    implicit val resultReader = new DoubleReader {}

    context.scheduleActivity(activity, TaskList("ignore"), timeout, value)
  }

  private[this] val timeout = ActivityTimeout(
    Seconds.seconds(1),
    Seconds.seconds(1),
    Seconds.seconds(1),
    Seconds.seconds(1))
}

final class SumActivity extends ActivityHandler[Seq[Double], Double] with SeqDoubleReader with DoubleWriter {
  override def handleActivity(context: ActivityService, input: Seq[Double]): Double = {
    input.sum
  }
}
