package stig.mesos

import scala.collection.mutable

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import org.joda.time.DateTime
import org.apache.mesos.Protos

import stig.Decider
import stig.model.Workflow
import stig.model.WorkflowEvent
import stig.model.Decision
import stig.Stig

object DecisionActor {
  def props(scheduler: ActorRef, deciders: Map[Workflow, Decider]): Props = {
    Props(new DecisionActor(scheduler, deciders))
  }
}

case class WorkflowCompleted(result: Array[Byte])
case class StartWorkflow(workflow: Workflow, input: Array[Byte])
case class TaskStarted(identity: String, scheduledEventId: Int, taskId: Protos.TaskID)
case class TaskCompleted(taskId: Protos.TaskID, result: Array[Byte])
case class LaunchTask(
  scheduledEventId: Int,
  decision: Decision.ScheduleActivityTask)

final class DecisionActor(
    scheduler: ActorRef,
    deciders: Map[Workflow, Decider]) extends Actor {

  // TODO: Huge hack fix this ASAP
  private[this] var client = Option.empty[ActorRef]

  private[this] val id = IdGenerator()
  private[this] var events = mutable.Queue.empty[WorkflowEvent]
  private[this] val outstandingTasks = mutable.Map.empty[Protos.TaskID, (Int, Int)]
  private[this] var previousId = 0
  private[this] var newId = 0

  def receive: Receive = {
    case StartWorkflow(workflow, input) =>
      client = Some(sender)
      startWorkflow(workflow, input)

    case TaskStarted(identity, scheduledEventId, taskId) =>
      taskStarted(identity, scheduledEventId, taskId)

    case TaskCompleted(taskId, result) =>
      taskCompleted(taskId, result)
  }

  def taskCompleted(taskId: Protos.TaskID, result: Array[Byte]): Unit = {
    for ((scheduledEventId, startedEventId) <- outstandingTasks.remove(taskId)) {
      events += WorkflowEvent.ActivityTaskCompleted(
        id(),
        DateTime.now(),
        scheduledEventId,
        startedEventId,
        result)

      // Tell the decider about it
      previousId = newId
      executeDecider()
    }
  }

  def taskStarted(identity: String, scheduledEventId: Int, taskId: Protos.TaskID): Unit = {
    val startedEventId = id()
    events += WorkflowEvent.ActivityTaskStarted(
      startedEventId,
      DateTime.now(),
      identity,
      scheduledEventId)

    outstandingTasks += ((taskId, (scheduledEventId, startedEventId)))
  }

  def startWorkflow(workflow: Workflow, input: Array[Byte]): Unit = {
    events += WorkflowEvent.WorkflowExecutionStarted(id(), DateTime.now(), workflow, input)

    previousId = 0
    executeDecider()
  }

  def executeDecider(): Unit = {
    val scheduledId = id()
    events += WorkflowEvent.DecisionTaskScheduled(scheduledId, DateTime.now())

    newId = id()
    events += WorkflowEvent.DecisionTaskStarted(newId, DateTime.now(), scheduledId)

    val decisions = Stig.makeDecisions(previousId, newId, events, deciders)

    val completedId = id()
    events += WorkflowEvent.DecisionTaskCompleted(completedId, DateTime.now(), scheduledId, newId)

    decisions.foreach { decision =>
      // If the decision is to schedule a task then send to scheduler
      decision match {
        case decision @ Decision.ScheduleActivityTask(activity, taskList, taskId, input, timeout) =>
          // Update the history
          val scheduledEventId = id()
          events += WorkflowEvent.ActivityTaskScheduled(
            scheduledEventId,
            DateTime.now(),
            taskId,
            activity,
            completedId,
            timeout,
            input,
            taskList)

          scheduler ! LaunchTask(scheduledEventId, decision)

        case Decision.CompleteWorkflow(result) =>
          client.foreach { value =>
            value ! WorkflowCompleted(result)
          }

        case _ =>
        // TODO: Ignore everything else for now.
      }
    }
  }
}

object IdGenerator {
  def apply(): IdGenerator = new IdGenerator()
}

final class IdGenerator {
  private[this] var currentId = 0

  def apply(): Int = {
    currentId += 1
    currentId
  }
}
