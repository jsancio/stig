package stig.mesos

import java.io.BufferedWriter
import java.io.ObjectInputStream

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Success
import scala.util.Failure

import org.apache.mesos.Executor
import org.apache.mesos.ExecutorDriver
import org.apache.mesos.Protos
import com.google.protobuf.ByteString

import stig.model.Activity
import stig.model.Decision
import stig.Stig
import stig.Worker

final class StigExecutor(
    writer: BufferedWriter,
    workers: Map[Activity, Worker])(
        implicit executor: ExecutionContext) extends Executor {

  def disconnected(driver: ExecutorDriver): Unit = {}

  def error(driver: ExecutorDriver, message: String): Unit = {}

  def frameworkMessage(driver: ExecutorDriver, data: Array[Byte]): Unit = {}

  def killTask(driver: ExecutorDriver, taskId: Protos.TaskID): Unit = {}

  def launchTask(driver: ExecutorDriver, task: Protos.TaskInfo): Unit = {
    writer.write("----------------------------------------------------------------------\n")
    writer.write(s"received the following task: $task\n")
    writer.write("----------------------------------------------------------------------\n")
    writer.flush()

    Future {
      val deserializer = new ObjectInputStream(task.getData.newInput)
      val activityTask = deserializer.readObject().asInstanceOf[Decision.ScheduleActivityTask]
      deserializer.close()

      Stig.executeActivity(activityTask.activity, activityTask.input, workers)
    } onComplete {
      case Success(result) =>
        // For now lets shut complete the task
        val status = Protos.TaskStatus.newBuilder
          .setTaskId(task.getTaskId)
          .setState(Protos.TaskState.TASK_FINISHED)
          .setData(ByteString.copyFrom(result))
          .build

        driver.sendStatusUpdate(status)
      case Failure(error) =>
        val status = Protos.TaskStatus.newBuilder
          .setTaskId(task.getTaskId)
          .setState(Protos.TaskState.TASK_FAILED)
          .build

        driver.sendStatusUpdate(status)
    }
  }

  def registered(
    driver: ExecutorDriver,
    executorInfo: Protos.ExecutorInfo,
    frameworkInfo: Protos.FrameworkInfo,
    slaveInfo: Protos.SlaveInfo): Unit = {}

  def reregistered(driver: ExecutorDriver, slaveInfo: Protos.SlaveInfo): Unit = {}

  def shutdown(driver: ExecutorDriver): Unit = {
    writer.close()
  }
}
