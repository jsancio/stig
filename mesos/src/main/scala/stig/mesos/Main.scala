package stig.mesos

import java.nio.charset.Charset

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import org.apache.mesos.MesosSchedulerDriver
import org.apache.mesos.Protos
import org.joda.time.Seconds

import stig.DeciderService
import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.TaskList
import stig.model.Workflow
import stig.mesos.demo.DemoDecider
import stig.mesos.demo.SeqDoubleWriter

object Main extends App {
  val url = "zk://localhost:2181/mesos"
  val name = "StigFramework"
  val user = "jose"
  val workflow = Workflow("stig-mesos", "1.0")

  val actorSystem = ActorSystem("stig-mesos")

  val schedulerActor = actorSystem.actorOf(SchedulerActor.props)

  val deciderActor = actorSystem.actorOf(
    DecisionActor.props(schedulerActor, Map((workflow, new DemoDecider))))

  val driver = new MesosSchedulerDriver(
    new StigScheduler(schedulerActor),
    Protos.FrameworkInfo.newBuilder.setName(name).setUser(user).build(),
    url)
  driver.start()

  // Shutdown the Mesos driver when the actor system terminates
  actorSystem.registerOnTermination {
    driver.stop()
  }

  val seqDoubleWriter = new SeqDoubleWriter {}
  val input = (1 to 100).map(_.toDouble).toArray

  implicit val timeout = Timeout(5.minute)
  val WorkflowCompleted(result) = Await.result(
    (deciderActor ? StartWorkflow(
      workflow,
      seqDoubleWriter.write(input))).mapTo[WorkflowCompleted],
    timeout.duration)

  println(s"----------------------------------------------------------------------")
  println(s"Result: ${new String(result, Charset.forName("UTF-8"))}")
  println(s"----------------------------------------------------------------------")

  actorSystem.shutdown()

  // TODO: We need to start the decider actor(s)
  // TODO: When should this shutdown?

  def scalarResource(
    name: String,
    value: Double,
    role: String = "*"): Protos.Resource = {

    Protos.Resource.newBuilder
      .setType(Protos.Value.Type.SCALAR)
      .setName(name)
      .setScalar(Protos.Value.Scalar.newBuilder.setValue(value))
      .setRole(role)
      .build
  }
}
