package stig.mesos

import java.lang.management.ManagementFactory
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.FileSystems
import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext

import org.apache.mesos.MesosExecutorDriver

import stig.ActivityService
import stig.mesos.demo.SquareRootActivity
import stig.mesos.demo.SumActivity
import stig.model.Activity

object ExecutorMain extends App {
  val driver = {
    val path = FileSystems.getDefault().getPath(
      "/tmp",
      "stig_mesos",
      ManagementFactory.getRuntimeMXBean().getName())

    new MesosExecutorDriver(
      new StigExecutor(
        Files.newBufferedWriter(path, Charset.forName("UTF-8")),
        Map(
          (SquareRootActivity.activity, new SquareRootActivity),
          (SumActivity.activity, new SumActivity)))(
        ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())))
  }

  driver.run()
}
