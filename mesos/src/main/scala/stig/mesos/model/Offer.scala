package stig.mesos.model

import org.apache.mesos.Protos
import org.apache.mesos.SchedulerDriver

// Note that SchedulerDriver is not serializable but it is okay for how we plan to deploy this
case class Offer(driver: SchedulerDriver, offer: Protos.Offer)

// TODO: move this out of here
case class TaskStatus(driver: SchedulerDriver, offer: Protos.TaskStatus)
