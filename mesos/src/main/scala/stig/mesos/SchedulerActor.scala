package stig.mesos

import java.io.ObjectOutputStream

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.JavaConverters.seqAsJavaListConverter
import scala.collection.mutable

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import com.google.protobuf.ByteString
import org.apache.mesos.Protos

import stig.mesos.model.Offer
import stig.mesos.model.TaskStatus
import stig.model.Decision

object SchedulerActor {
  val props: Props = Props(new SchedulerActor)
}

final class SchedulerActor extends Actor {
  private[this] val tasks = mutable.Queue.empty[(ActorRef, LaunchTask)]
  private[this] val id = IdGenerator()

  private[this] val command_line = "java -cp /home/jose/project/stig/mesos/target/scala-2.11/stig-mesos-assembly-0.1.jar -Djava.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib:/usr/local/lib stig.mesos.ExecutorMain"

  // TODO: pure hack but let's do this for now
  private[this] var decider = Option.empty[ActorRef]

  def receive: Receive = {
    case TaskStatus(driver, status) =>
      // TODO: tell the decider that the task finished
      // TODO: we only have one decider so this is for okay: we need to fix this asap
      decider.foreach { value =>
        value ! TaskCompleted(status.getTaskId, status.getData.toByteArray)
      }

    case Offer(driver, offer) =>
      // TODO: if we have any pending task that need to get schedule let's go through those
      // TODO: if not then lets just remember the offer
      // TODO: we need to have code that declines offers after some time

      // TODO: Do we have a pending task? Just grab the first offer
      val allTasks = take(offer, tasks)

      if (allTasks.isEmpty) {
        // No tasks to run decline offer
        println("task queue is empty")
        driver.declineOffer(offer.getId)
      } else {
        val mesosTasks = for {
          (decisionSender, LaunchTask(scheduleEventId, decision)) <- allTasks
        } yield {
          val task = createTask(offer, decision)

          // TODO: pretty big hack but okay for now...
          // Tell the sender that schedule the task
          // Identity should include executor information
          decisionSender ! TaskStarted(task.getSlaveId.getValue, scheduleEventId, task.getTaskId)

          task
        }

        // Launch the task
        println(s"launching ${mesosTasks.length}")
        driver.launchTasks(mutable.ArrayBuffer(offer.getId).asJava, mesosTasks.asJava)
      }

    case launchTask: LaunchTask =>
      decider = Some(sender)
      tasks += ((sender, launchTask))
  }

  private def createTask(
    offer: Protos.Offer,
    decision: Decision.ScheduleActivityTask): Protos.TaskInfo = {

    // Serialize the decision so we can send it to the slave
    // TODO: change this to use ByteString
    val buffer = ByteString.newOutput()
    val serializer = new ObjectOutputStream(buffer)
    serializer.writeObject(decision)
    serializer.close()

    // TODO: come up with a better executor id
    val executorId = "executor-id-1"
    val taskId = s"task-${id()}"

    Protos.TaskInfo.newBuilder
      .setName(taskId)
      .setTaskId(Protos.TaskID.newBuilder.setValue(taskId))
      .setSlaveId(offer.getSlaveId)
      .addResources(Main.scalarResource("cpus", 0.1))
      .addResources(Main.scalarResource("mem", 1.0))
      .setExecutor(Protos.ExecutorInfo.newBuilder
        .setExecutorId(Protos.ExecutorID.newBuilder.setValue(executorId))
        .setFrameworkId(offer.getFrameworkId)
        .setCommand(Protos.CommandInfo.newBuilder.setValue(command_line)))
      .setData(buffer.toByteString)
      .build
  }

  private def take[A](offer: Protos.Offer, queue: mutable.Queue[A]): Seq[A] = {
    val number = offer.getResourcesList.asScala.find { resource =>
      resource.getName == "cpus"
    } map { resource =>
      (resource.getScalar.getValue.toDouble * 10).toInt
    } getOrElse (0)

    println(s"We can launch $number tasks")

    for (_ <- 1 to Math.min(number, queue.length)) yield {
      queue.dequeue()
    }
  }
}
