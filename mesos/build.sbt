name := "stig-mesos"

version := "0.1"

scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
  "org.apache.mesos" % "mesos" % "0.21.0",
  "com.typesafe.akka" %% "akka-actor" % "2.3.8")

javaOptions += "-Djava.library.path=%s:%s".format(
  sys.props("java.library.path"),
  "/usr/local/lib")

fork in run := true

scalariformSettings
