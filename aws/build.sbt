name := "stig-aws"

version := "0.1"

scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
  "com.amazonaws" % "aws-java-sdk" % "1.9.4",
  "org.apache.httpcomponents" % "httpclient" % "4.3.6" % "runtime",
  "org.specs2" %% "specs2" % "2.4.9" % "test")

scalariformSettings
