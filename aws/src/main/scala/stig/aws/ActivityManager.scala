package stig.aws

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.control.NonFatal

import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow
import com.amazonaws.services.simpleworkflow.model.ActivityTask
import com.amazonaws.services.simpleworkflow.model.PollForActivityTaskRequest
import com.amazonaws.services.simpleworkflow.model.RespondActivityTaskCompletedRequest
import com.amazonaws.services.simpleworkflow.model.RespondActivityTaskFailedRequest
import com.amazonaws.services.simpleworkflow.model.UnknownResourceException
import com.codahale.metrics.Gauge
import com.codahale.metrics.MetricRegistry
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.codec.binary.Base64

import stig.ActivityWorkerNotFoundException
import stig.aws.util.actorName
import stig.aws.util.countActivityPendingTasks
import stig.aws.util.ErrorCode
import stig.aws.util.StigThreadFactory
import stig.model.Activity
import stig.model.Domain
import stig.model.TaskList
import stig.serial.exceptionReaderWriter
import stig.Stig
import stig.Worker
import StigConverter.ActivityTypeConverter
import StigConverter.TaskListConverter

final class ActivityManager(
    numberOfActors: Int,
    domain: Domain,
    taskList: TaskList,
    client: AmazonSimpleWorkflow,
    workers: Map[Activity, Worker],
    metrics: MetricRegistry) extends LazyLogging with AutoCloseable {

  private[this] val executor = Executors.newScheduledThreadPool(
    numberOfActors,
    new StigThreadFactory(taskList.name))

  private[this] val waiting = metrics.counter(
    MetricRegistry.name(getClass, "waitingActors"))

  private[this] val running = metrics.counter(
    MetricRegistry.name(getClass, "runningActors", taskList.name))

  private[this] val throttle = metrics.meter(
    MetricRegistry.name(getClass, "awsThrottling", ErrorCode.ThrottlingException))

  private[this] val polling = metrics.meter(
    MetricRegistry.name(getClass, "awsPolling"))

  private[this] val queueSize = metrics.register(
    MetricRegistry.name(getClass, "actorQueue", taskList.name),
    new Gauge[Int] {
      override def getValue(): Int = {
        countActivityPendingTasks(client, domain, taskList).count
      }
    })

  // Start polling
  executor.execute(new Runner)

  override def close(): Unit = {
    executor.shutdown()
  }

  private final class Runner extends Runnable {
    case class ActivityState(
      taskToken: String,
      id: String,
      activity: Activity,
      input: Array[Byte])

    override def run(): Unit = {
      val name = actorName()

      logger.info(s"Waiting for an activity task at ($domain, $taskList)")

      for (activityState <- pollAndReschedule(name)) {
        // Increment the running counter before running the activity
        running.inc()

        logger.info(s"Processing activity (${activityState.activity}): ${activityState.id}")

        try {
          val result = Stig.executeActivity(activityState.activity, activityState.input, workers)

          logger.info(s"Finished activity ${activityState.id}")

          completeActivityTask(activityState.taskToken, result)
        } catch {
          case exception: ActivityWorkerNotFoundException =>
            logger.error(s"Unknown activity: ${activityState.activity}", exception)
            failActivityTask(activityState.taskToken, "Missing activity", exception)

          case NonFatal(exception) =>
            logger.error(
              s"Unhandle activity exception (${activityState.activity}: ${activityState.input}",
              exception)
            failActivityTask(activityState.taskToken, "Unknown error", exception)
        } finally {
          // Decrease the running counter after running activity
          running.dec()
        }
      }
    }

    private def pollAndReschedule(name: String): Option[ActivityState] = {
      // Deal with polling and re-scheduling
      try {
        val result = pollForActivityTask(name, domain, taskList)

        // Schedule another poll request
        executor.execute(new Runner)

        result
      } catch {
        case e: AmazonServiceException if e.getErrorCode == ErrorCode.ThrottlingException =>
          // Getting throttled: 1. mark meter, 2. log and 3. delay
          throttle.mark()

          val time = 500
          logger.warn(s"$name call to AWS was throttled. Delaying poll by $time ms")

          executor.schedule(new Runner, time, TimeUnit.MILLISECONDS)

          None

        case e: AmazonClientException =>
          /* Sometime we get this exception for some strange reason. This solution is not ideal
           * it doesn't solve the problem. Let's just log an error and retry.
           */

          val time = 500
          logger.error(s"Suspecting connection reset. Deplay poll by $time ms", e)

          executor.schedule(new Runner, time, TimeUnit.MILLISECONDS)

          None

        case e: Throwable =>
          logger.error("Unhandled exception:", e)
          throw e
      }
    }

    private def pollForActivityTask(
      name: String,
      domain: Domain,
      taskList: TaskList): Option[ActivityState] = {

      // Increase the waiting counter before we poll for tasks
      waiting.inc()

      polling.mark()

      val activityTask = try {
        client.pollForActivityTask(
          new PollForActivityTaskRequest()
            .withDomain(domain.name)
            .withTaskList(taskList.asAws)
            .withIdentity(name))
      } finally {
        // Decrease the waiting counter after we polled for tasks
        waiting.dec()
      }

      Option(activityTask.getTaskToken).map { taskToken =>
        ActivityState(
          taskToken,
          activityTask.getActivityId,
          activityTask.getActivityType.asStig,
          Base64.decodeBase64(activityTask.getInput))
      }
    }

    private def completeActivityTask(taskToken: String, result: Array[Byte]): Unit = {
      try {
        client.respondActivityTaskCompleted(
          new RespondActivityTaskCompletedRequest()
            .withTaskToken(taskToken)
            .withResult(Base64.encodeBase64String(result)))
      } catch {
        case exception: UnknownResourceException =>
          // Protect ourselves against missing resources
          // which could have happened when the task timed out
          logger.warn(s"Unable to complete the task: ${exception.toString}")
      }
    }

    private def failActivityTask(
      taskToken: String,
      reason: String,
      exception: Throwable): Unit = {

      // Fail the test with the serialized exception.
      try {
        client.respondActivityTaskFailed(
          new RespondActivityTaskFailedRequest()
            .withTaskToken(taskToken)
            .withReason(reason)
            .withDetails(Base64.encodeBase64String(exceptionReaderWriter.write(exception))))
      } catch {
        case exception: UnknownResourceException =>
          // Protect ourself against missing resources which could happend when the task timedout
          logger.warn(s"Unable to fail the task: ${exception.toString}")
      }
    }
  }
}
