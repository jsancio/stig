package stig.aws.util

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.ThreadFactory

object StigThreadFactory {
  private val poolNumber = new AtomicInteger(1)
}

final class StigThreadFactory(poolName: String) extends ThreadFactory {
  import StigThreadFactory.poolNumber

  private[this] val group = Option(System.getSecurityManager) match {
    case Some(securityManager) => securityManager.getThreadGroup
    case None => Thread.currentThread().getThreadGroup
  }

  private[this] val threadNumber = new AtomicInteger(1)

  private[this] val namePrefix = s"stig-$poolName-${poolNumber.getAndIncrement}-thread-"

  override def newThread(runnable: Runnable): Thread = {
    val thread = new Thread(group, runnable, s"${namePrefix}${threadNumber.getAndIncrement}", 0)

    if (thread.isDaemon) thread.setDaemon(false)
    if (thread.getPriority != Thread.NORM_PRIORITY) thread.setPriority(Thread.NORM_PRIORITY)

    thread
  }
}
