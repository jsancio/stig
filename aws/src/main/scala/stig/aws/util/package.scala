package stig.aws

import java.lang.management.ManagementFactory

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow
import com.amazonaws.services.simpleworkflow.model.CountPendingActivityTasksRequest
import com.amazonaws.services.simpleworkflow.model.CountPendingDecisionTasksRequest

import stig.model.Domain
import stig.model.PendingTaskCount
import stig.model.TaskList
import StigConverter.SWFPendingTaskCountConverter
import StigConverter.TaskListConverter

package object util {
  def actorName(): String = {
    val vmName = ManagementFactory.getRuntimeMXBean.getName
    val threadId = Thread.currentThread.getName

    s"$threadId@$vmName"
  }

  def countActivityPendingTasks(
    client: AmazonSimpleWorkflow,
    domain: Domain,
    taskList: TaskList): PendingTaskCount = {

    client.countPendingActivityTasks(new CountPendingActivityTasksRequest()
      .withDomain(domain.name)
      .withTaskList(taskList.asAws)).asStig
  }

  def countDecisionPendingTasks(
    client: AmazonSimpleWorkflow,
    domain: Domain,
    taskList: TaskList): PendingTaskCount = {

    client.countPendingDecisionTasks(new CountPendingDecisionTasksRequest()
      .withDomain(domain.name)
      .withTaskList(taskList.asAws)).asStig
  }
}
