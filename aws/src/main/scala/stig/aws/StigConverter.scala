package stig.aws

import scala.collection.JavaConverters.asScalaBufferConverter

import com.amazonaws.services.simpleworkflow.model.ActivityType
import com.amazonaws.services.simpleworkflow.model.CompleteWorkflowExecutionDecisionAttributes
import com.amazonaws.services.simpleworkflow.model.ContinueAsNewWorkflowExecutionDecisionAttributes
import com.amazonaws.services.simpleworkflow.model.DecisionType
import com.amazonaws.services.simpleworkflow.model.ExecutionTimeFilter
import com.amazonaws.services.simpleworkflow.model.FailWorkflowExecutionDecisionAttributes
import com.amazonaws.services.simpleworkflow.model.HistoryEvent
import com.amazonaws.services.simpleworkflow.model.ScheduleActivityTaskDecisionAttributes
import com.amazonaws.services.simpleworkflow.model.StartChildWorkflowExecutionDecisionAttributes
import com.amazonaws.services.simpleworkflow.model.StartTimerDecisionAttributes
import com.amazonaws.services.simpleworkflow.model.WorkflowExecutionCount
import com.amazonaws.services.simpleworkflow.model.WorkflowType
import com.amazonaws.services.simpleworkflow.model.{ ChildPolicy => SWFChildPolicy }
import com.amazonaws.services.simpleworkflow.model.{ CloseStatus => SWFCloseStatus }
import com.amazonaws.services.simpleworkflow.model.{ Decision => SWFDecision }
import com.amazonaws.services.simpleworkflow.model.{ PendingTaskCount => SWFPendingTaskCount }
import com.amazonaws.services.simpleworkflow.model.{ TaskList => SWFTaskList }
import com.amazonaws.services.simpleworkflow.model.{ WorkflowExecution => SWFWorkflowExecution }
import com.amazonaws.services.simpleworkflow.model.{ WorkflowExecutionInfo => SWFWorkflowInfo }
import com.amazonaws.services.simpleworkflow.model.{ WorkflowExecutionInfos => SWFWorkflowInfos }
import org.apache.commons.codec.binary.Base64
import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.Seconds

import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.ChildPolicy
import stig.model.CloseStatus
import stig.model.Decision
import stig.model.PendingTaskCount
import stig.model.ScheduleActivityFailedCause
import stig.model.TaskList
import stig.model.Workflow
import stig.model.WorkflowCount
import stig.model.WorkflowEvent
import stig.model.WorkflowExecution
import stig.model.WorkflowExecutionInfo
import stig.model.WorkflowExecutionInfos

object StigConverter {
  implicit class ChildPolicyConverter(val childPolicy: ChildPolicy.ChildPolicy) extends AnyVal {
    def asAws: SWFChildPolicy = {
      childPolicy match {
        case ChildPolicy.Terminate => SWFChildPolicy.TERMINATE
        case ChildPolicy.RequestCancel => SWFChildPolicy.REQUEST_CANCEL
        case ChildPolicy.Abandon => SWFChildPolicy.ABANDON
      }
    }
  }

  implicit class SWFChildPolicyConverter(val swfChildPolicy: SWFChildPolicy) extends AnyVal {
    def asStig: ChildPolicy.ChildPolicy = {
      swfChildPolicy match {
        case SWFChildPolicy.TERMINATE => ChildPolicy.Terminate
        case SWFChildPolicy.REQUEST_CANCEL => ChildPolicy.RequestCancel
        case SWFChildPolicy.ABANDON => ChildPolicy.Abandon
      }
    }
  }

  implicit class ActivityTypeConverter(val activityType: ActivityType) extends AnyVal {
    def asStig: Activity = {
      Activity(activityType.getName, activityType.getVersion)
    }
  }

  implicit class ActivityConverter(val activity: Activity) extends AnyVal {
    def asAws: ActivityType = {
      new ActivityType()
        .withName(activity.name)
        .withVersion(activity.version)
    }
  }

  implicit class SWFTaskListConverter(val taskList: SWFTaskList) extends AnyVal {
    def asStig: TaskList = {
      TaskList(taskList.getName)
    }
  }

  implicit class TaskListConverter(val taskList: TaskList) extends AnyVal {
    def asAws: SWFTaskList = {
      new SWFTaskList()
        .withName(taskList.name)
    }
  }

  implicit class DecisionConverter(val decision: Decision) extends AnyVal {
    def asAws: SWFDecision = decision match {
      case Decision.ContinueAsNewWorkflow(input) =>
        convertContinueAsNewWorkflow(input)

      case Decision.StartChildWorkflow(id, workflow, input) =>
        convertStartChildWorkflow(id, workflow, input)

      case Decision.FailWorkflow(reason, details) =>
        convertFailWorkflow(reason, details)

      case Decision.CompleteWorkflow(result) =>
        convertCompleteWorkflow(result)

      case Decision.ScheduleActivityTask(activity, taskList, id, input, timeout) =>
        convertScheduleActivityTask(activity, taskList, id, input, timeout)

      case Decision.StartTimer(id, timeout) =>
        convertStartTimer(id, timeout)
    }

    private def convertContinueAsNewWorkflow(input: Array[Byte]): SWFDecision = {
      new SWFDecision()
        .withDecisionType(DecisionType.ContinueAsNewWorkflowExecution)
        .withContinueAsNewWorkflowExecutionDecisionAttributes(
          new ContinueAsNewWorkflowExecutionDecisionAttributes()
            .withInput(Base64.encodeBase64String(input)))
    }

    private def convertStartChildWorkflow(
      id: String,
      workflow: Workflow,
      input: Array[Byte]): SWFDecision = {
      new SWFDecision()
        .withDecisionType(DecisionType.StartChildWorkflowExecution)
        .withStartChildWorkflowExecutionDecisionAttributes(
          new StartChildWorkflowExecutionDecisionAttributes()
            .withInput(Base64.encodeBase64String(input))
            .withWorkflowId(id)
            .withWorkflowType(new WorkflowType()
              .withName(workflow.name)
              .withVersion(workflow.version)))
    }

    private def convertFailWorkflow(reason: String, details: Array[Byte]): SWFDecision = {
      new SWFDecision()
        .withDecisionType(DecisionType.FailWorkflowExecution)
        .withFailWorkflowExecutionDecisionAttributes(
          new FailWorkflowExecutionDecisionAttributes()
            .withDetails(Base64.encodeBase64String(details))
            .withReason(reason))
    }

    private def convertCompleteWorkflow(result: Array[Byte]): SWFDecision = {
      new SWFDecision()
        .withDecisionType(DecisionType.CompleteWorkflowExecution)
        .withCompleteWorkflowExecutionDecisionAttributes(
          new CompleteWorkflowExecutionDecisionAttributes()
            .withResult(Base64.encodeBase64String(result)))
    }

    private def convertScheduleActivityTask(
      activity: Activity,
      taskList: TaskList,
      id: Int,
      input: Array[Byte],
      timeout: ActivityTimeout): SWFDecision = {

      new SWFDecision()
        .withDecisionType(DecisionType.ScheduleActivityTask)
        .withScheduleActivityTaskDecisionAttributes(
          new ScheduleActivityTaskDecisionAttributes()
            .withActivityId(id.toString)
            .withActivityType(activity.asAws)
            .withInput(Base64.encodeBase64String(input))
            .withStartToCloseTimeout(timeout.startToClose.getSeconds.toString)
            .withScheduleToStartTimeout(timeout.scheduleToStart.getSeconds.toString)
            .withScheduleToCloseTimeout(timeout.scheduleToClose.getSeconds.toString)
            .withHeartbeatTimeout(timeout.heartbeat.getSeconds.toString)
            .withTaskList(taskList.asAws))
    }

    private def convertStartTimer(id: Int, timeout: Seconds): SWFDecision = {
      new SWFDecision()
        .withDecisionType(DecisionType.StartTimer)
        .withStartTimerDecisionAttributes(
          new StartTimerDecisionAttributes()
            .withTimerId(id.toString)
            .withStartToFireTimeout(timeout.getSeconds.toString))
    }
  }

  implicit class HistoryEventConverter(val event: HistoryEvent) extends AnyVal {
    private def convertWorkflowExecutionStart: WorkflowEvent = {
      val attributes = event.getWorkflowExecutionStartedEventAttributes
      WorkflowEvent.WorkflowExecutionStarted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getWorkflowType.asStig,
        Base64.decodeBase64(attributes.getInput))
    }

    private def convertWorkflowExecutionFailed: WorkflowEvent = {
      val attributes = event.getWorkflowExecutionFailedEventAttributes

      WorkflowEvent.WorkflowExecutionFailed(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getReason,
        attributes.getDetails,
        attributes.getDecisionTaskCompletedEventId)
    }

    private def convertWorkflowExecutionCompleted: WorkflowEvent = {
      val attributes = event.getWorkflowExecutionCompletedEventAttributes

      WorkflowEvent.WorkflowExecutionCompleted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        Base64.decodeBase64(attributes.getResult),
        attributes.getDecisionTaskCompletedEventId)
    }

    private def convertDecisionTaskScheduled: WorkflowEvent = {
      WorkflowEvent.DecisionTaskScheduled(
        event.getEventId,
        new DateTime(event.getEventTimestamp))
    }

    private def convertDecisionTaskStarted: WorkflowEvent = {
      val attributes = event.getDecisionTaskStartedEventAttributes
      WorkflowEvent.DecisionTaskStarted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getScheduledEventId)
    }

    private def convertDecisionTaskCompleted: WorkflowEvent = {
      val attributes = event.getDecisionTaskCompletedEventAttributes
      WorkflowEvent.DecisionTaskCompleted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getScheduledEventId,
        attributes.getStartedEventId)
    }

    private def convertDecisionTaskTimedOut: WorkflowEvent = {
      val attributes = event.getDecisionTaskTimedOutEventAttributes
      WorkflowEvent.DecisionTaskTimedOut(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getTimeoutType,
        attributes.getScheduledEventId,
        attributes.getStartedEventId)
    }

    private def convertActivityTaskScheduled: WorkflowEvent = {
      val attributes = event.getActivityTaskScheduledEventAttributes
      WorkflowEvent.ActivityTaskScheduled(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getActivityId.toInt,
        attributes.getActivityType.asStig,
        attributes.getDecisionTaskCompletedEventId,
        ActivityTimeout(
          Seconds.seconds(attributes.getScheduleToStartTimeout.toInt),
          Seconds.seconds(attributes.getScheduleToCloseTimeout.toInt),
          Seconds.seconds(attributes.getStartToCloseTimeout.toInt),
          Seconds.seconds(attributes.getHeartbeatTimeout.toInt)),
        Base64.decodeBase64(attributes.getInput),
        attributes.getTaskList.asStig)
    }

    private def convertActivityTaskStarted: WorkflowEvent = {
      val attributes = event.getActivityTaskStartedEventAttributes
      WorkflowEvent.ActivityTaskStarted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getIdentity,
        attributes.getScheduledEventId)
    }

    private def convertActivityTaskCompleted: WorkflowEvent = {
      val attributes = event.getActivityTaskCompletedEventAttributes
      WorkflowEvent.ActivityTaskCompleted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getScheduledEventId,
        attributes.getStartedEventId,
        Base64.decodeBase64(attributes.getResult))
    }

    private def convertActivityTaskFailed: WorkflowEvent = {
      val attributes = event.getActivityTaskFailedEventAttributes
      WorkflowEvent.ActivityTaskFailed(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getScheduledEventId,
        attributes.getStartedEventId,
        attributes.getReason,
        Base64.decodeBase64(attributes.getDetails))
    }

    private def convertScheduleActivityTaskFailed: WorkflowEvent = {
      val attributes = event.getScheduleActivityTaskFailedEventAttributes
      WorkflowEvent.ScheduleActivityTaskFailed(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getActivityType.asStig,
        attributes.getActivityId.toInt,
        ScheduleActivityFailedCause.withName(attributes.getCause),
        attributes.getDecisionTaskCompletedEventId)
    }

    private def convertFailWorkflowExecutionFailed: WorkflowEvent = {
      val attributes = event.getFailWorkflowExecutionFailedEventAttributes

      WorkflowEvent.FailWorkflowExecutionFailed(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getCause,
        attributes.getDecisionTaskCompletedEventId)
    }

    private def convertActivityTaskTimedOut: WorkflowEvent = {
      val attributes = event.getActivityTaskTimedOutEventAttributes
      WorkflowEvent.ActivityTaskTimedOut(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getScheduledEventId,
        attributes.getStartedEventId,
        attributes.getTimeoutType,
        Option(attributes.getDetails))
    }

    private def convertTimerStarted: WorkflowEvent = {
      val attributes = event.getTimerStartedEventAttributes
      WorkflowEvent.TimerStarted(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getTimerId.toInt,
        Seconds.seconds(attributes.getStartToFireTimeout.toInt),
        attributes.getDecisionTaskCompletedEventId)
    }

    private def convertTimerFired: WorkflowEvent = {
      val attributes = event.getTimerFiredEventAttributes
      WorkflowEvent.TimerFired(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getTimerId.toInt,
        attributes.getStartedEventId)
    }

    private def convertStartTimerFailed: WorkflowEvent = {
      val attributes = event.getStartTimerFailedEventAttributes
      WorkflowEvent.StartTimerFailed(
        event.getEventId,
        new DateTime(event.getEventTimestamp),
        attributes.getTimerId.toInt,
        attributes.getCause,
        attributes.getDecisionTaskCompletedEventId)
    }

    // scalastyle:off cyclomatic.complexity
    def asStig: WorkflowEvent = event.getEventType match {
      case "WorkflowExecutionStarted" => convertWorkflowExecutionStart
      case "WorkflowExecutionFailed" => convertWorkflowExecutionFailed
      case "WorkflowExecutionCompleted" => convertWorkflowExecutionCompleted
      case "DecisionTaskScheduled" => convertDecisionTaskScheduled
      case "DecisionTaskStarted" => convertDecisionTaskStarted
      case "DecisionTaskCompleted" => convertDecisionTaskCompleted
      case "DecisionTaskTimedOut" => convertDecisionTaskTimedOut
      case "ActivityTaskScheduled" => convertActivityTaskScheduled
      case "ActivityTaskStarted" => convertActivityTaskStarted
      case "ActivityTaskCompleted" => convertActivityTaskCompleted
      case "ActivityTaskFailed" => convertActivityTaskFailed
      case "ActivityTaskTimedOut" => convertActivityTaskTimedOut
      case "ScheduleActivityTaskFailed" => convertScheduleActivityTaskFailed
      case "FailWorkflowExecutionFailed" => convertFailWorkflowExecutionFailed
      case "TimerStarted" => convertTimerStarted
      case "TimerFired" => convertTimerFired
      case "StartTimerFailed" => convertStartTimerFailed
    }
    // scalastyle:on cyclomatic.complexity
  }

  implicit class WorkflowTypeConverter(val workflow: WorkflowType) extends AnyVal {
    def asStig: Workflow = {
      Workflow(workflow.getName, workflow.getVersion)
    }
  }

  implicit class WorkflowConverter(val workflow: Workflow) extends AnyVal {
    def asAws: WorkflowType = {
      new WorkflowType()
        .withName(workflow.name)
        .withVersion(workflow.version)
    }
  }

  implicit class CloseStatusConverter(val closeStatus: CloseStatus) extends AnyVal {
    def asAws: SWFCloseStatus = {
      closeStatus match {
        case CloseStatus.Canceled => SWFCloseStatus.CANCELED
        case CloseStatus.Completed => SWFCloseStatus.COMPLETED
        case CloseStatus.ContinuedAsNew => SWFCloseStatus.CONTINUED_AS_NEW
        case CloseStatus.Failed => SWFCloseStatus.FAILED
        case CloseStatus.Terminated => SWFCloseStatus.TERMINATED
        case CloseStatus.TimedOut => SWFCloseStatus.TIMED_OUT
      }
    }
  }

  implicit class SWFCloseStatusConverter(val closeStatus: SWFCloseStatus) extends AnyVal {
    def asStig: CloseStatus = {
      closeStatus match {
        case SWFCloseStatus.CANCELED => CloseStatus.Canceled
        case SWFCloseStatus.COMPLETED => CloseStatus.Completed
        case SWFCloseStatus.CONTINUED_AS_NEW => CloseStatus.ContinuedAsNew
        case SWFCloseStatus.FAILED => CloseStatus.Failed
        case SWFCloseStatus.TERMINATED => CloseStatus.Terminated
        case SWFCloseStatus.TIMED_OUT => CloseStatus.TimedOut
      }
    }
  }

  implicit class SWFWorkflowCountConverter(val count: WorkflowExecutionCount) extends AnyVal {
    def asStig: WorkflowCount = {
      WorkflowCount(count.getCount, count.getTruncated)
    }
  }

  implicit class WorkflowExecutionConverter(
      val execution: WorkflowExecution) extends AnyVal {
    def asAws: SWFWorkflowExecution = {
      new SWFWorkflowExecution()
        .withRunId(execution.runId)
        .withWorkflowId(execution.workflowId)
    }
  }

  implicit class SWFWorkflowExecutionConverter(
      val execution: SWFWorkflowExecution) extends AnyVal {
    def asStig: WorkflowExecution = {
      WorkflowExecution(execution.getRunId, execution.getWorkflowId)
    }
  }

  implicit class SWFWorkflowExecutionInfoConverter(
      val info: SWFWorkflowInfo) extends AnyVal {
    def asStig: WorkflowExecutionInfo = {
      WorkflowExecutionInfo(
        info.getExecution.asStig,
        Option(info.getCloseStatus).map(SWFCloseStatus.fromValue(_).asStig),
        info.getExecutionStatus,
        new DateTime(info.getStartTimestamp),
        Option(info.getCloseTimestamp).map(new DateTime(_)))
    }
  }

  implicit class SWFWorkflowExecutionInfosConverter(
      val infos: SWFWorkflowInfos) extends AnyVal {
    def asStig: WorkflowExecutionInfos = {
      WorkflowExecutionInfos(
        infos.getExecutionInfos.asScala.map(_.asStig),
        Option(infos.getNextPageToken))
    }
  }

  implicit class IntervalConverter(val interval: Interval) extends AnyVal {
    def asAws: ExecutionTimeFilter = {
      new ExecutionTimeFilter()
        .withOldestDate(interval.getStart.toDate)
        .withLatestDate(interval.getEnd.toDate)
    }
  }

  implicit class SWFPendingTaskCountConverter(val count: SWFPendingTaskCount) extends AnyVal {
    def asStig: PendingTaskCount = {
      PendingTaskCount(count.getTruncated, count.getCount)
    }
  }
}
