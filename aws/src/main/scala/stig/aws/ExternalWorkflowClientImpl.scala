package stig.aws

import scala.annotation.tailrec
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.JavaConverters.seqAsJavaListConverter

import com.amazonaws.AmazonServiceException
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow
import com.amazonaws.services.simpleworkflow.model.CloseStatusFilter
import com.amazonaws.services.simpleworkflow.model.CountClosedWorkflowExecutionsRequest
import com.amazonaws.services.simpleworkflow.model.DomainAlreadyExistsException
import com.amazonaws.services.simpleworkflow.model.GetWorkflowExecutionHistoryRequest
import com.amazonaws.services.simpleworkflow.model.ListClosedWorkflowExecutionsRequest
import com.amazonaws.services.simpleworkflow.model.ListOpenWorkflowExecutionsRequest
import com.amazonaws.services.simpleworkflow.model.RegisterActivityTypeRequest
import com.amazonaws.services.simpleworkflow.model.RegisterDomainRequest
import com.amazonaws.services.simpleworkflow.model.RegisterWorkflowTypeRequest
import com.amazonaws.services.simpleworkflow.model.StartWorkflowExecutionRequest
import com.amazonaws.services.simpleworkflow.model.TypeAlreadyExistsException
import com.amazonaws.services.simpleworkflow.model.WorkflowExecutionAlreadyStartedException
import com.amazonaws.services.simpleworkflow.model.WorkflowExecutionFilter
import com.amazonaws.services.simpleworkflow.model.WorkflowExecutionInfo
import com.amazonaws.services.simpleworkflow.model.WorkflowTypeFilter
import com.amazonaws.services.simpleworkflow.model.{ TagFilter => SWFTagFilter }
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.codec.binary.Base64
import org.joda.time.Days
import org.joda.time.Interval

import stig.aws.util.countActivityPendingTasks
import stig.aws.util.countDecisionPendingTasks
import stig.aws.util.ErrorCode
import stig.ExternalWorkflowClient
import stig.model.Activity
import stig.model.ChildPolicy
import stig.model.CloseStatus
import stig.model.Domain
import stig.model.ExecutionFilter
import stig.model.IntervalFilter
import stig.model.IntervalType
import stig.model.PendingTaskCount
import stig.model.StatusFilter
import stig.model.TagFilter
import stig.model.TaskList
import stig.model.TypeFilter
import stig.model.Workflow
import stig.model.WorkflowCount
import stig.model.WorkflowEvent
import stig.model.WorkflowExecution
import stig.model.WorkflowExecutionInfos
import stig.model.WorkflowFilter
import stig.model.WorkflowTimeout
import stig.serial.Writer
import stig.ThrottlingException
import stig.WorkflowAlreadyStartedException
import StigConverter.ChildPolicyConverter
import StigConverter.CloseStatusConverter
import StigConverter.HistoryEventConverter
import StigConverter.IntervalConverter
import StigConverter.SWFWorkflowCountConverter
import StigConverter.SWFWorkflowExecutionInfosConverter
import StigConverter.TaskListConverter
import StigConverter.WorkflowConverter
import StigConverter.WorkflowExecutionConverter

final class ExternalWorkflowClientImpl(client: AmazonSimpleWorkflow)
    extends ExternalWorkflowClient with LazyLogging {

  // scalastyle:ignore parameter.number
  override def startWorkflow[Input](
    domain: Domain,
    workflow: Workflow,
    input: Input,
    tags: Seq[String],
    taskList: TaskList,
    timeout: WorkflowTimeout,
    childPolicy: ChildPolicy.ChildPolicy,
    workflowId: String)(implicit inputWriter: Writer[Input]): String = {

    try {
      client.startWorkflowExecution(new StartWorkflowExecutionRequest()
        .withDomain(domain.name)
        .withExecutionStartToCloseTimeout(timeout.executionStartToClose.getSeconds.toString)
        .withTaskStartToCloseTimeout(timeout.taskStartToClose.getSeconds.toString)
        .withInput(Base64.encodeBase64String(inputWriter.write(input)))
        .withWorkflowId(workflowId)
        .withTagList(tags.asJava)
        .withTaskList(taskList.asAws)
        .withChildPolicy(childPolicy.asAws)
        .withWorkflowType(workflow.asAws)).getRunId
    } catch {
      case e: WorkflowExecutionAlreadyStartedException =>
        throw new WorkflowAlreadyStartedException(workflowId, e)

      case e: AmazonServiceException if e.getErrorCode == ErrorCode.ThrottlingException =>
        throw new ThrottlingException("Call to start a workflow was throttled", e)
    }
  }

  override def registerDomain(domain: Domain, description: String, retention: Days): Unit = {
    try {
      client.registerDomain(new RegisterDomainRequest()
        .withName(domain.name)
        .withDescription(description)
        .withWorkflowExecutionRetentionPeriodInDays(retention.getDays.toString))
    } catch {
      case failure: DomainAlreadyExistsException =>
        logger.info(s"Domain $domain already exists: $failure")
    }
  }

  override def registerWorkflow(domain: Domain, workflow: Workflow, description: String): Unit = {
    try {
      client.registerWorkflowType(new RegisterWorkflowTypeRequest()
        .withDomain(domain.name)
        .withDescription(description)
        .withName(workflow.name)
        .withVersion(workflow.version))
    } catch {
      case failure: TypeAlreadyExistsException =>
        logger.info(s"Workflow $workflow in $domain already exists: $failure")
    }
  }

  override def registerActivity(domain: Domain, activity: Activity, description: String): Unit = {
    try {
      client.registerActivityType(new RegisterActivityTypeRequest()
        .withDomain(domain.name)
        .withDescription(description)
        .withName(activity.name)
        .withVersion(activity.version))
    } catch {
      case failure: TypeAlreadyExistsException =>
        logger.info(s"Activity $activity in $domain already exists: $failure")
    }
  }

  override def countClosedWorkflows(
    domain: Domain,
    intervalFilter: IntervalFilter,
    workflowFilter: Option[WorkflowFilter] = None): WorkflowCount = {

    val request = new CountClosedWorkflowExecutionsRequest().withDomain(domain.name)

    intervalFilter.filter match {
      case IntervalType.Start =>
        request.setStartTimeFilter(intervalFilter.interval.asAws)
      case IntervalType.Close =>
        request.setCloseTimeFilter(intervalFilter.interval.asAws)
    }

    for (filter <- workflowFilter) {
      filter match {
        case ExecutionFilter(id) =>
          request.setExecutionFilter(new WorkflowExecutionFilter().withWorkflowId(id))
        case TagFilter(tag) =>
          request.setTagFilter(new SWFTagFilter().withTag(tag))
        case TypeFilter(workflow) =>
          request.setTypeFilter(new WorkflowTypeFilter()
            .withName(workflow.name)
            .withVersion(workflow.version))
        case StatusFilter(status) =>
          request.setCloseStatusFilter(new CloseStatusFilter().withStatus(status.asAws))
      }
    }

    client.countClosedWorkflowExecutions(request).asStig
  }

  override def listOpenWorkflow(
    domain: Domain,
    startTimeFilter: Interval,
    maximumPageSize: Int,
    workflowFilter: Option[WorkflowFilter], // TODO: Don't support closed status filter
    nextPageToken: Option[String]): WorkflowExecutionInfos = {

    val request = new ListOpenWorkflowExecutionsRequest()
      .withDomain(domain.name)
      .withMaximumPageSize(maximumPageSize)
      .withStartTimeFilter(startTimeFilter.asAws)

    for (filter <- workflowFilter) {
      filter match {
        case ExecutionFilter(id) =>
          request.setExecutionFilter(new WorkflowExecutionFilter().withWorkflowId(id))
        case TagFilter(tag) =>
          request.setTagFilter(new SWFTagFilter().withTag(tag))
        case TypeFilter(workflow) =>
          request.setTypeFilter(new WorkflowTypeFilter()
            .withName(workflow.name)
            .withVersion(workflow.version))
        case StatusFilter(status) =>
          // Ignore this... This won't happend when we fix our API
          logger.error(s"Close status filter not allow when listing open executions: $status")
      }
    }

    for (token <- nextPageToken) {
      request.setNextPageToken(token)
    }

    client.listOpenWorkflowExecutions(request).asStig
  }

  override def listClosedWorkflow(
    domain: Domain,
    intervalFilter: IntervalFilter,
    maximumPageSize: Int,
    workflowFilter: Option[WorkflowFilter],
    nextPageToken: Option[String]): WorkflowExecutionInfos = {

    val request = new ListClosedWorkflowExecutionsRequest()
      .withDomain(domain.name)
      .withMaximumPageSize(maximumPageSize)

    intervalFilter.filter match {
      case IntervalType.Start =>
        request.setStartTimeFilter(intervalFilter.interval.asAws)
      case IntervalType.Close =>
        request.setCloseTimeFilter(intervalFilter.interval.asAws)
    }

    for (filter <- workflowFilter) {
      filter match {
        case ExecutionFilter(id) =>
          request.setExecutionFilter(new WorkflowExecutionFilter().withWorkflowId(id))
        case TagFilter(tag) =>
          request.setTagFilter(new SWFTagFilter().withTag(tag))
        case TypeFilter(workflow) =>
          request.setTypeFilter(new WorkflowTypeFilter()
            .withName(workflow.name)
            .withVersion(workflow.version))
        case StatusFilter(status) =>
          request.setCloseStatusFilter(new CloseStatusFilter().withStatus(status.asAws))
      }
    }

    for (token <- nextPageToken) {
      request.setNextPageToken(token)
    }

    client.listClosedWorkflowExecutions(request).asStig
  }

  override def getWorkflowHistory(
    domain: Domain,
    execution: WorkflowExecution): Seq[WorkflowEvent] = {

    getWorkflowHistoryPart(Vector.empty, domain, execution, None)
  }

  @tailrec
  private def getWorkflowHistoryPart(
    history: Vector[WorkflowEvent],
    domain: Domain,
    execution: WorkflowExecution,
    maybeNextPageToken: Option[String]): Vector[WorkflowEvent] = {

    val request = new GetWorkflowExecutionHistoryRequest()
      .withDomain(domain.name)
      .withExecution(execution.asAws)

    for (token <- maybeNextPageToken) {
      request.setNextPageToken(token)
    }

    val result = client.getWorkflowExecutionHistory(request)

    val newHistory = history ++ result.getEvents.asScala.map(_.asStig)

    Option(result.getNextPageToken) match {
      case nextPageToken @ Some(_) =>
        getWorkflowHistoryPart(newHistory, domain, execution, nextPageToken)

      case None =>
        newHistory
    }
  }

  override def countPendingActivityTasks(domain: Domain, taskList: TaskList): PendingTaskCount = {
    countActivityPendingTasks(client, domain, taskList)
  }

  override def countPendingDecisionTasks(domain: Domain, taskList: TaskList): PendingTaskCount = {
    countDecisionPendingTasks(client, domain, taskList)
  }
}
