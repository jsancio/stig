package stig.aws.script

import java.nio.charset.Charset

import scala.io.StdIn

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflowClient
import com.codahale.metrics.MetricRegistry
import org.joda.time.Seconds

import stig.ActivityFailedException
import stig.ActivityService
import stig.ActivityTimedOutException
import stig.aws.ActivityManager
import stig.aws.DecisionManager
import stig.DeciderService
import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.Domain
import stig.model.TaskList
import stig.model.Workflow
import stig.ScheduleActivityFailedException
import stig.serial.stringReader
import stig.serial.stringWriter

object SimpleTest extends App {
  object Activities {
    val simpleActivity = Activity("simple-activity", "1.0")
    val faultyActivity = Activity("faulty-activity", "1.0")
    val missingActivity = Activity("missing-activity", "1.0")
    val timeoutActivity = Activity("timeout-activity", "1.0")
  }

  val client = new AmazonSimpleWorkflowClient()
  client.setEndpoint("swf.us-west-1.amazonaws.com")

  val domain = Domain("test-domain")
  val taskList = TaskList("test_task")
  val timeout = {
    val seconds = 5

    ActivityTimeout(
      Seconds.seconds(seconds),
      Seconds.seconds(seconds),
      Seconds.seconds(seconds),
      Seconds.seconds(seconds))
  }

  val defaultRegistry = new MetricRegistry()

  val decisionManager = new DecisionManager(
    1,
    1,
    domain,
    taskList,
    client,
    Map(Workflow("simple-workflow", "1.1") -> decider),
    defaultRegistry)

  val activityManager = new ActivityManager(
    1,
    domain,
    taskList,
    client,
    Map(
      Activities.simpleActivity -> activity,
      Activities.faultyActivity -> faultyActivity,
      Activities.timeoutActivity -> timeoutActivity),
    defaultRegistry)

  println("Press any key to shutdown...") // scalastyle:ignore
  StdIn.readLine()
  activityManager.close()
  decisionManager.close()

  def decider(context: DeciderService, input: Array[Byte]): Unit = {
    context.scheduleActivity(Activities.missingActivity, taskList, timeout, input).onFailure {
      case failure: ScheduleActivityFailedException =>
        // TODO: what should we do with the failure variable?
        context.scheduleActivity(
          Activities.timeoutActivity,
          taskList,
          timeout,
          input).onFailure {
            case failure: ActivityTimedOutException =>
              // TODO: what should we do with the failure variable?
              context.scheduleActivity(
                Activities.faultyActivity,
                taskList,
                timeout,
                input).onFailure {
                  case failure: ActivityFailedException =>
                    // TODO: what should we do with the failure variable?
                    context.startTimer(Seconds.seconds(3)).foreach { _ =>
                      context.scheduleActivity(
                        Activities.simpleActivity,
                        taskList,
                        timeout,
                        input).foreach { result =>
                          context.completeWorkflow(s"decider's result: $result")
                        }
                    }
                }
          }
    }
  }

  def activity(context: ActivityService, input: Array[Byte]): Array[Byte] = {
    val string = new String(input, Charset.forName("UTF-8"))
    s"activity result: $string".getBytes(Charset.forName("UTF-8"))
  }

  def faultyActivity(context: ActivityService, input: Array[Byte]): Array[Byte] = {
    throw new IllegalStateException("This activity always fails")
  }

  def timeoutActivity(context: ActivityService, input: Array[Byte]): Array[Byte] = {
    val string = new String(input, Charset.forName("UTF-8"))

    // Sleep for twice as long at the timeout
    Thread.sleep((timeout.startToClose.getSeconds * 1000) + 2000)
    s"should never have this result: $string".getBytes(Charset.forName("UTF-8"))
  }
}
