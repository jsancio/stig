package stig.aws

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

import scala.annotation.tailrec
import scala.collection.JavaConverters.asJavaCollectionConverter
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.control.NonFatal

import com.amazonaws.AmazonServiceException
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow
import com.amazonaws.services.simpleworkflow.model.PollForDecisionTaskRequest
import com.amazonaws.services.simpleworkflow.model.RespondDecisionTaskCompletedRequest
import com.amazonaws.services.simpleworkflow.model.UnknownResourceException
import com.codahale.metrics.Gauge
import com.codahale.metrics.MetricRegistry
import com.typesafe.scalalogging.LazyLogging

import stig.aws.util.actorName
import stig.aws.util.countDecisionPendingTasks
import stig.aws.util.ErrorCode
import stig.Decider
import stig.DeciderNotFoundException
import stig.model.Decision
import stig.model.Domain
import stig.model.TaskList
import stig.model.Workflow
import stig.model.WorkflowEvent
import stig.serial.exceptionReaderWriter
import stig.Stig
import StigConverter.DecisionConverter
import StigConverter.HistoryEventConverter
import StigConverter.TaskListConverter

final class DecisionManager(
    numberOfActors: Int,
    concurrency: Int,
    domain: Domain,
    taskList: TaskList,
    client: AmazonSimpleWorkflow,
    deciders: Map[Workflow, Decider],
    metrics: MetricRegistry) extends LazyLogging with AutoCloseable {

  require(
    concurrency <= numberOfActors,
    s"Concurrency ($concurrency) must be less than then number of actors ($numberOfActors)")

  private[this] val executor = Executors.newScheduledThreadPool(numberOfActors)
  private[this] val initDelay = 500
  private[this] val maxDelay = 5000

  private[this] val waiting = metrics.counter(
    MetricRegistry.name(getClass, "waitingActors"))

  private[this] val running = metrics.counter(
    MetricRegistry.name(getClass, "runningActors"))

  private[this] val throttle = metrics.meter(
    MetricRegistry.name(getClass, "awsThrottling", ErrorCode.ThrottlingException))

  private[this] val polling = metrics.meter(
    MetricRegistry.name(getClass, "awsPolling"))

  private[this] val activitySchedule = metrics.meter(
    MetricRegistry.name(getClass, "activitySchedule"))

  private[this] val queueSize = metrics.register(
    MetricRegistry.name(getClass, "actorQueue", taskList.name),
    new Gauge[Int] {
      override def getValue(): Int = {
        countDecisionPendingTasks(client, domain, taskList).count
      }
    })

  // Start polling
  for (_ <- 1 to concurrency) {
    executor.execute(new Runner(initDelay))
  }

  override def close(): Unit = {
    executor.shutdown()
  }

  private final class Runner(delay: Int) extends Runnable {
    case class WorkflowState(
      taskToken: String,
      runId: String,
      executionId: String,
      previousStartedEventId: Long,
      startedEventId: Long,
      events: Vector[WorkflowEvent])

    override def run(): Unit = {
      val name = actorName()

      logger.info("Waiting on a decision task")

      for (state <- pollAndReschedule(name)) {
        // Increment the running counter before running the decider
        running.inc()

        logger.info(s"Processing decision task: ${state.runId}")

        try {
          val decisions = Stig.makeDecisions(
            state.previousStartedEventId,
            state.startedEventId,
            state.events,
            deciders)

          completeDecisionTask(state.taskToken, decisions)
        } catch {
          case exception: DeciderNotFoundException =>
            logger.error("Unknown decider", exception)
            failWorkflow(state.taskToken, "Missing Decider", exception)

          case NonFatal(exception) =>
            logger.error(s"Caught non-fatal exception for (${state.executionId})", exception)
            failWorkflow(state.taskToken, "Decider threw an exception", exception)
        } finally {
          // Decrease the running counter after running decider
          running.dec()
        }
      }
    }

    private def pollAndReschedule(name: String): Option[WorkflowState] = {
      // Deal with polling and re-scheduling
      try {
        val result = pollForWorkflowState(name)

        // Schedule another poll request
        executor.execute(new Runner(initDelay))

        result
      } catch {
        case e: AmazonServiceException if e.getErrorCode == ErrorCode.ThrottlingException =>
          // Getting throttled: 1. mark meter, 2. log and 3. delay poll
          throttle.mark()

          logger.warn(s"$name call to AWS was throttled. Delaying poll for $delay ms")

          executor.schedule(
            new Runner(math.min(delay * 2, maxDelay)),
            delay,
            TimeUnit.MILLISECONDS)

          None

        case e: Throwable =>
          logger.error("Unhandled exception", e)
          throw e
      }
    }

    private def pollForWorkflowState(name: String): Option[WorkflowState] = {
      // Increase the waiting counter before we poll for decisions
      waiting.inc()

      try {
        pollForDecisionTask(Vector.empty, name, None)
      } finally {
        // Decrease the waiting counter after we polled for tasks
        waiting.dec()
      }
    }

    @tailrec
    private def pollForDecisionTask(
      decisions: Vector[WorkflowEvent],
      name: String,
      maybeNextPageToken: Option[String]): Option[WorkflowState] = {

      polling.mark()
      val decisionTask = client.pollForDecisionTask(createPollRequest(name, maybeNextPageToken))

      Option(decisionTask.getTaskToken) match {
        case None => None

        case Some(taskToken) =>
          val newDecisions = decisions ++ decisionTask.getEvents.asScala.map(_.asStig)

          Option(decisionTask.getNextPageToken) match {
            case nextPageToken @ Some(_) =>
              pollForDecisionTask(newDecisions, name, nextPageToken)

            case None =>
              Option(
                WorkflowState(
                  taskToken,
                  decisionTask.getWorkflowExecution.getRunId,
                  decisionTask.getWorkflowExecution.getWorkflowId,
                  decisionTask.getPreviousStartedEventId,
                  decisionTask.getStartedEventId,
                  newDecisions))
          }
      }
    }

    private def createPollRequest(
      name: String,
      maybeNextPageToken: Option[String]): PollForDecisionTaskRequest = {
      val request = new PollForDecisionTaskRequest()
        .withDomain(domain.name)
        .withTaskList(taskList.asAws)
        .withIdentity(name)

      for (nextPageToken <- maybeNextPageToken) {
        request.setNextPageToken(nextPageToken)
      }

      request
    }

    private def completeDecisionTask(taskToken: String, decisions: Iterable[Decision]): Unit = {
      val swfDecision = new RespondDecisionTaskCompletedRequest()
        .withTaskToken(taskToken)

      measureActivityScheduleRate(decisions)

      if (decisions.nonEmpty) {
        swfDecision.setDecisions(
          decisions.map(_.asAws).asJavaCollection)
      }

      /* TODO: Need to protect ourselves against this containing bad decisions and call throwing
     * an exception.
     */
      logger.debug(s"Send AWS SWF the following decisions: $decisions")

      try {
        client.respondDecisionTaskCompleted(swfDecision)
      } catch {
        case exception: UnknownResourceException =>
          logger.warn(s"Resource doesn't exists. Most likely workflow timedout.", exception)
      }
    }

    private def failWorkflow(taskToken: String, reason: String, exception: Throwable): Unit = {
      completeDecisionTask(
        taskToken,
        Seq(Decision.FailWorkflow(reason, exceptionReaderWriter.write(exception))))
    }

    private def measureActivityScheduleRate(decisions: Iterable[Decision]): Unit = {
      for (decision <- decisions) {
        decision match {
          case _: Decision.ScheduleActivityTask =>
            activitySchedule.mark()
          case _ => // Ignore. Only mark meter for schedule activity task events
        }
      }
    }
  }
}
