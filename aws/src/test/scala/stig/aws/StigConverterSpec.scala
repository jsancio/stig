package stig.aws

import com.amazonaws.services.simpleworkflow.model.ActivityTaskCompletedEventAttributes
import com.amazonaws.services.simpleworkflow.model.ActivityTaskFailedEventAttributes
import com.amazonaws.services.simpleworkflow.model.ActivityTaskScheduledEventAttributes
import com.amazonaws.services.simpleworkflow.model.ActivityTaskStartedEventAttributes
import com.amazonaws.services.simpleworkflow.model.ActivityTaskTimedOutEventAttributes
import com.amazonaws.services.simpleworkflow.model.ActivityType
import com.amazonaws.services.simpleworkflow.model.DecisionTaskCompletedEventAttributes
import com.amazonaws.services.simpleworkflow.model.DecisionTaskScheduledEventAttributes
import com.amazonaws.services.simpleworkflow.model.DecisionTaskStartedEventAttributes
import com.amazonaws.services.simpleworkflow.model.DecisionTaskTimedOutEventAttributes
import com.amazonaws.services.simpleworkflow.model.DecisionTaskTimeoutType
import com.amazonaws.services.simpleworkflow.model.DecisionType
import com.amazonaws.services.simpleworkflow.model.EventType
import com.amazonaws.services.simpleworkflow.model.ExecutionTimeFilter
import com.amazonaws.services.simpleworkflow.model.FailWorkflowExecutionFailedEventAttributes
import com.amazonaws.services.simpleworkflow.model.HistoryEvent
import com.amazonaws.services.simpleworkflow.model.ScheduleActivityTaskFailedCause
import com.amazonaws.services.simpleworkflow.model.ScheduleActivityTaskFailedEventAttributes
import com.amazonaws.services.simpleworkflow.model.StartTimerFailedEventAttributes
import com.amazonaws.services.simpleworkflow.model.TimerFiredEventAttributes
import com.amazonaws.services.simpleworkflow.model.TimerStartedEventAttributes
import com.amazonaws.services.simpleworkflow.model.WorkflowExecutionFailedEventAttributes
import com.amazonaws.services.simpleworkflow.model.WorkflowExecutionStartedEventAttributes
import com.amazonaws.services.simpleworkflow.model.WorkflowType
import com.amazonaws.services.simpleworkflow.model.{ ChildPolicy => SWFChildPolicy }
import com.amazonaws.services.simpleworkflow.model.{ CloseStatus => SWFCloseStatus }
import com.amazonaws.services.simpleworkflow.model.{ TaskList => SWFTaskList }
import com.amazonaws.services.simpleworkflow.model.{ WorkflowExecution => SWFWorkflowExecution }
import com.amazonaws.services.simpleworkflow.model.{ WorkflowExecutionInfo => SWFWorkflowExecutionInfo }
import com.amazonaws.services.simpleworkflow.model.{ WorkflowExecutionInfos => SWFWorkflowExecutionInfos }
import org.apache.commons.codec.binary.Base64
import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.Seconds
import org.specs2.mutable

import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.ChildPolicy
import stig.model.CloseStatus
import stig.model.Decision
import stig.model.ScheduleActivityFailedCause
import stig.model.TaskList
import stig.model.Workflow
import stig.model.WorkflowEvent
import stig.model.WorkflowExecution
import stig.model.WorkflowExecutionInfo
import stig.model.WorkflowExecutionInfos
import StigConverter.ActivityConverter
import StigConverter.ActivityTypeConverter
import StigConverter.ChildPolicyConverter
import StigConverter.CloseStatusConverter
import StigConverter.DecisionConverter
import StigConverter.HistoryEventConverter
import StigConverter.IntervalConverter
import StigConverter.SWFChildPolicyConverter
import StigConverter.SWFCloseStatusConverter
import StigConverter.SWFTaskListConverter
import StigConverter.SWFWorkflowExecutionConverter
import StigConverter.SWFWorkflowExecutionInfoConverter
import StigConverter.SWFWorkflowExecutionInfosConverter
import StigConverter.TaskListConverter
import StigConverter.WorkflowConverter
import StigConverter.WorkflowExecutionConverter
import StigConverter.WorkflowTypeConverter

final class StigConverterSpec extends mutable.Specification {
  "ChildPolicy conversion" should {
    "convert from AWS" in {
      SWFChildPolicy.values().map(_.asStig).toSet must_== ChildPolicy.values
    }

    "convert to AWS" in {
      ChildPolicy.values.map(_.asAws).toSet must_== SWFChildPolicy.values.toSet
    }
  }

  "TaskList conversion" should {
    "convert from AWS" in {
      val name = "tasklist-name"
      val stigTaskList = new SWFTaskList()
        .withName(name)
        .asStig

      stigTaskList must_== TaskList(name)
    }

    "convert to AWS" in {
      val name = "tasklist-name"
      val awsTaskList = TaskList(name).asAws

      awsTaskList.getName must_== name
    }
  }

  "Activity conversion" should {
    "convert from AWS" in {
      val name = "activity-name"
      val version = "1.0"

      val stigActivity = new ActivityType()
        .withName(name)
        .withVersion(version).asStig

      stigActivity must_== Activity(name, version)
    }

    "convert to AWS" in {
      val name = "activity-name"
      val version = "1.0"

      val awsActivity = Activity(name, version).asAws

      (awsActivity.getName must_== name) and (
        awsActivity.getVersion must_== version)
    }
  }

  "Decision conversion" should {
    "convert continue as new to AWS" in {
      val input = "test input".getBytes("UTF-8")

      val awsDecision = Decision.ContinueAsNewWorkflow(input).asAws

      (awsDecision.getDecisionType must_==
        DecisionType.ContinueAsNewWorkflowExecution.toString) and (
          Base64.decodeBase64(
            awsDecision.getContinueAsNewWorkflowExecutionDecisionAttributes.getInput) must_==
            input)
    }

    "convert start child workflow to AWS" in {
      val input = "test input".getBytes("UTF-8")
      val id = "id"
      val name = "workflow-name"
      val version = "1.0"

      val awsDecision = Decision.StartChildWorkflow(id, Workflow(name, version), input).asAws

      val attributes = awsDecision.getStartChildWorkflowExecutionDecisionAttributes
      (awsDecision.getDecisionType must_== DecisionType.StartChildWorkflowExecution.toString) and (
        Base64.decodeBase64(attributes.getInput) must_== input) and (
          attributes.getWorkflowId must_== id) and (
            attributes.getWorkflowType.getName must_== name) and (
              attributes.getWorkflowType.getVersion must_== version)

    }

    "convert fail workflow to AWS" in {
      val reason = "test reason"
      val details = "test details".getBytes("UTF-8")

      val awsDecision = Decision.FailWorkflow(reason, details).asAws

      val attributes = awsDecision.getFailWorkflowExecutionDecisionAttributes
      (awsDecision.getDecisionType must_== DecisionType.FailWorkflowExecution.toString) and (
        attributes.getReason must_== reason) and (
          Base64.decodeBase64(attributes.getDetails) must_== details)
    }

    "convert complete workflow to AWS" in {
      val result = "test result".getBytes("UTF-8")

      val awsDecision = Decision.CompleteWorkflow(result).asAws

      (awsDecision.getDecisionType must_== DecisionType.CompleteWorkflowExecution.toString) and (
        Base64.decodeBase64(
          awsDecision.getCompleteWorkflowExecutionDecisionAttributes.getResult) must_== result)
    }

    "convert schedule activity task to AWS" in {
      val name = "activity-name"
      val version = "1.0"
      val taskList = TaskList("test-tasklist")
      val id = 11
      val input = "test input".getBytes("UTF-8")
      val timeout = ActivityTimeout(
        Seconds.seconds(1),
        Seconds.seconds(2),
        Seconds.seconds(3),
        Seconds.seconds(4))

      val awsDecision = Decision.ScheduleActivityTask(
        Activity(name, version),
        taskList,
        id,
        input,
        timeout).asAws

      val attributes = awsDecision.getScheduleActivityTaskDecisionAttributes
      (awsDecision.getDecisionType must_== DecisionType.ScheduleActivityTask.toString) and (
        attributes.getActivityId must_== id.toString) and (
          attributes.getActivityType.getName must_== name) and (
            attributes.getActivityType.getVersion must_== version) and (
              Base64.decodeBase64(attributes.getInput) must_== input) and (
                attributes.getTaskList.asStig must_== taskList) and (
                  attributes.getScheduleToStartTimeout must_==
                  timeout.scheduleToStart.getSeconds.toString) and (
                    attributes.getScheduleToCloseTimeout must_==
                    timeout.scheduleToClose.getSeconds.toString) and (
                      attributes.getStartToCloseTimeout must_==
                      timeout.startToClose.getSeconds.toString) and (
                        attributes.getHeartbeatTimeout must_==
                        timeout.heartbeat.getSeconds.toString)
    }

    "convert start time to AWS" in {
      val id = 12
      val timeout = Seconds.seconds(1)

      val awsDecision = Decision.StartTimer(id, timeout).asAws

      val attributes = awsDecision.getStartTimerDecisionAttributes
      (awsDecision.getDecisionType must_== DecisionType.StartTimer.toString) and (
        attributes.getTimerId must_== id.toString) and (
          attributes.getStartToFireTimeout must_== timeout.getSeconds.toString)
    }
  }

  "HistoryEvent conversion" should {
    val eventId = 11
    val timestamp = DateTime.now()

    "convert workflow execution started from AWS" in {
      val input = "test input".getBytes("UTF-8")
      val workflow = Workflow("test-workflow", "1.0")

      val stigEvent = new HistoryEvent()
        .withEventType(EventType.WorkflowExecutionStarted)
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withWorkflowExecutionStartedEventAttributes(
          new WorkflowExecutionStartedEventAttributes()
            .withInput(Base64.encodeBase64String(input))
            .withWorkflowType(workflow.asAws))
        .asStig.asInstanceOf[WorkflowEvent.WorkflowExecutionStarted]

      (stigEvent.id must_== eventId) and (
        stigEvent.timestamp must_== timestamp) and (
          stigEvent.workflow must_== workflow) and (
            stigEvent.input must_== input)

    }

    "convert workflow execution failed from AWS" in {
      val reason = "reason"
      val details = "details"
      val eventId = 1L

      val stigEvent = new HistoryEvent()
        .withEventType(EventType.WorkflowExecutionFailed)
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withWorkflowExecutionFailedEventAttributes(
          new WorkflowExecutionFailedEventAttributes()
            .withReason(reason)
            .withDetails(details)
            .withDecisionTaskCompletedEventId(eventId)).asStig

      stigEvent must_== WorkflowEvent.WorkflowExecutionFailed(
        eventId,
        timestamp,
        reason,
        details,
        eventId)
    }

    "convert decision task scheduled from AWS" in {
      val stigEvent = new HistoryEvent()
        .withEventType(EventType.DecisionTaskScheduled)
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withDecisionTaskScheduledEventAttributes(
          new DecisionTaskScheduledEventAttributes()).asStig

      stigEvent must_== WorkflowEvent.DecisionTaskScheduled(eventId, timestamp)
    }

    "convert decision task started from AWS" in {
      val scheduledId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.DecisionTaskStarted)
        .withDecisionTaskStartedEventAttributes(
          new DecisionTaskStartedEventAttributes()
            .withScheduledEventId(scheduledId)).asStig

      stigEvent must_== WorkflowEvent.DecisionTaskStarted(eventId, timestamp, scheduledId)
    }

    "convert decision task completed from AWS" in {
      val scheduledId = 9
      val startedId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.DecisionTaskCompleted)
        .withDecisionTaskCompletedEventAttributes(
          new DecisionTaskCompletedEventAttributes()
            .withScheduledEventId(scheduledId)
            .withStartedEventId(startedId)).asStig

      stigEvent must_== WorkflowEvent.DecisionTaskCompleted(
        eventId,
        timestamp,
        scheduledId,
        startedId)
    }

    "convert decision task timed out from AWS" in {
      val scheduledId = 9
      val startedId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.DecisionTaskTimedOut)
        .withDecisionTaskTimedOutEventAttributes(
          new DecisionTaskTimedOutEventAttributes()
            .withScheduledEventId(scheduledId)
            .withStartedEventId(startedId)
            .withTimeoutType(DecisionTaskTimeoutType.START_TO_CLOSE)).asStig

      stigEvent must_== WorkflowEvent.DecisionTaskTimedOut(
        eventId,
        timestamp,
        "START_TO_CLOSE",
        scheduledId,
        startedId)
    }

    "convert activity task scheduled from AWS" in {
      val activityId = 22
      val completedId = 33
      val activityTimeout = ActivityTimeout(
        Seconds.seconds(10),
        Seconds.seconds(11),
        Seconds.seconds(12),
        Seconds.seconds(13))
      val input = "input".getBytes("UTF-8")
      val activity = Activity("test-activity", "1.0")
      val taskList = TaskList("task-list")

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.ActivityTaskScheduled)
        .withActivityTaskScheduledEventAttributes(
          new ActivityTaskScheduledEventAttributes()
            .withActivityId(activityId.toString)
            .withActivityType(activity.asAws)
            .withDecisionTaskCompletedEventId(completedId)
            .withScheduleToStartTimeout(activityTimeout.scheduleToStart.getSeconds.toString)
            .withScheduleToCloseTimeout(activityTimeout.scheduleToClose.getSeconds.toString)
            .withStartToCloseTimeout(activityTimeout.startToClose.getSeconds.toString)
            .withHeartbeatTimeout(activityTimeout.heartbeat.getSeconds.toString)
            .withInput(Base64.encodeBase64String(input))
            .withTaskList(taskList.asAws))
        .asStig.asInstanceOf[WorkflowEvent.ActivityTaskScheduled]

      (stigEvent.id must_== eventId) and (
        stigEvent.timestamp must_== timestamp) and (
          stigEvent.activityId must_== activityId) and (
            stigEvent.activity must_== activity) and (
              stigEvent.decisionTaskCompletedEventId must_== completedId) and (
                stigEvent.activityTimeout must_== activityTimeout) and (
                  stigEvent.input must_== input) and (
                    stigEvent.taskList must_== taskList)
    }

    "convert activity task started from AWS" in {
      val scheduledId = 10
      val identity = "id"

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.ActivityTaskStarted)
        .withActivityTaskStartedEventAttributes(
          new ActivityTaskStartedEventAttributes()
            .withScheduledEventId(scheduledId)
            .withIdentity(identity)).asStig

      stigEvent must_== WorkflowEvent.ActivityTaskStarted(
        eventId,
        timestamp,
        identity,
        scheduledId)
    }

    "convert activity task completed from AWS" in {
      val scheduledId = 9
      val startedId = 10
      val result = "test result".getBytes("UTF-8")

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.ActivityTaskCompleted)
        .withActivityTaskCompletedEventAttributes(
          new ActivityTaskCompletedEventAttributes()
            .withScheduledEventId(scheduledId)
            .withStartedEventId(startedId)
            .withResult(Base64.encodeBase64String(result)))
        .asStig.asInstanceOf[WorkflowEvent.ActivityTaskCompleted]

      (stigEvent.id must_== eventId) and (
        stigEvent.timestamp must_== timestamp) and (
          stigEvent.scheduledEventId must_== scheduledId) and (
            stigEvent.startedEventId must_== startedId) and (
              stigEvent.result must_== result)
    }

    "convert activity task failed from AWS" in {
      val scheduledId = 9
      val startedId = 10
      val reason = "test reason"
      val details = "test details".getBytes("UTF-8")

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.ActivityTaskFailed)
        .withActivityTaskFailedEventAttributes(
          new ActivityTaskFailedEventAttributes()
            .withScheduledEventId(scheduledId)
            .withStartedEventId(startedId)
            .withReason(reason)
            .withDetails(Base64.encodeBase64String(details)))
        .asStig.asInstanceOf[WorkflowEvent.ActivityTaskFailed]

      (stigEvent.id must_== eventId) and (
        stigEvent.timestamp must_== timestamp) and (
          stigEvent.scheduledEventId must_== scheduledId) and (
            stigEvent.startedEventId must_== startedId) and (
              stigEvent.reason must_== reason) and (
                stigEvent.details must_== details)
    }

    "convert schedule task failed from AWS" in {
      val activity = Activity("test-activity", "1.0")
      val activityId = 1
      val decisionTaskCompletedId = 10

      val stigEvents = for (cause <- ScheduleActivityTaskFailedCause.values) yield {
        new HistoryEvent()
          .withEventId(eventId)
          .withEventTimestamp(timestamp.toDate)
          .withEventType(EventType.ScheduleActivityTaskFailed)
          .withScheduleActivityTaskFailedEventAttributes(
            new ScheduleActivityTaskFailedEventAttributes()
              .withActivityType(activity.asAws)
              .withActivityId(activityId.toString)
              .withCause(cause)
              .withDecisionTaskCompletedEventId(decisionTaskCompletedId)).asStig
      }

      val expected = for (cause <- ScheduleActivityTaskFailedCause.values) yield {
        WorkflowEvent.ScheduleActivityTaskFailed(
          eventId,
          timestamp,
          activity,
          activityId,
          ScheduleActivityFailedCause.withName(cause.name),
          decisionTaskCompletedId)
      }

      stigEvents must_== expected
    }

    "convert fail workflow execution failed from AWS" in {
      val activity = Activity("test-activity", "1.0")
      val activityId = 1
      val cause = "test cause"
      val decisionTaskCompletedId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.FailWorkflowExecutionFailed)
        .withFailWorkflowExecutionFailedEventAttributes(
          new FailWorkflowExecutionFailedEventAttributes()
            .withCause(cause)
            .withDecisionTaskCompletedEventId(decisionTaskCompletedId)).asStig

      stigEvent must_== WorkflowEvent.FailWorkflowExecutionFailed(
        eventId,
        timestamp,
        cause,
        decisionTaskCompletedId)
    }

    "convert activity task timedout from AWS" in {
      val details = "test details"
      val scheduledEventId = 9
      val startedEventId = 10
      val timeoutType = "timeout type"

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.ActivityTaskTimedOut)
        .withActivityTaskTimedOutEventAttributes(
          new ActivityTaskTimedOutEventAttributes()
            .withDetails(details)
            .withScheduledEventId(scheduledEventId)
            .withStartedEventId(startedEventId)
            .withTimeoutType(timeoutType)).asStig

      stigEvent must_== WorkflowEvent.ActivityTaskTimedOut(
        eventId,
        timestamp,
        scheduledEventId,
        startedEventId,
        timeoutType,
        Some(details))
    }

    "convert schedule timer from AWS" in {
      val timerId = 22
      val startToFireTimeout = Seconds.seconds(3)
      val decisionTaskCompletedEventId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.TimerStarted)
        .withTimerStartedEventAttributes(
          new TimerStartedEventAttributes()
            .withTimerId(timerId.toString)
            .withStartToFireTimeout(startToFireTimeout.getSeconds.toString)
            .withDecisionTaskCompletedEventId(decisionTaskCompletedEventId)).asStig

      stigEvent must_== WorkflowEvent.TimerStarted(
        eventId,
        timestamp,
        timerId,
        startToFireTimeout,
        decisionTaskCompletedEventId)
    }

    "convert timer fired from AWS" in {
      val timerId = 22
      val startedEventId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.TimerFired)
        .withTimerFiredEventAttributes(
          new TimerFiredEventAttributes()
            .withTimerId(timerId.toString)
            .withStartedEventId(startedEventId)).asStig

      stigEvent must_== WorkflowEvent.TimerFired(eventId, timestamp, timerId, startedEventId)
    }

    "convert start timer failed from AWS" in {
      val timerId = 22
      val cause = "test cause"
      val decisionTaskCompletedEventId = 10

      val stigEvent = new HistoryEvent()
        .withEventId(eventId)
        .withEventTimestamp(timestamp.toDate)
        .withEventType(EventType.StartTimerFailed)
        .withStartTimerFailedEventAttributes(
          new StartTimerFailedEventAttributes()
            .withTimerId(timerId.toString)
            .withCause(cause)
            .withDecisionTaskCompletedEventId(decisionTaskCompletedEventId)).asStig

      stigEvent must_== WorkflowEvent.StartTimerFailed(
        eventId,
        timestamp,
        timerId,
        cause,
        decisionTaskCompletedEventId)
    }
  }

  "WorkflowExecution" should {
    "convert from AWS" in {
      val runId = "random run id"
      val workflowId = "random execution id"

      val stigWorkflowExecution = new SWFWorkflowExecution()
        .withRunId(runId)
        .withWorkflowId(workflowId).asStig

      stigWorkflowExecution must_== WorkflowExecution(runId, workflowId)
    }

    "convert to AWS" in {
      val runId = "random run id"
      val workflowId = "random execution id"

      val awsWorkflowExecution = WorkflowExecution(runId, workflowId).asAws

      awsWorkflowExecution must_== new SWFWorkflowExecution()
        .withRunId(runId)
        .withWorkflowId(workflowId)
    }
  }

  "WorkflowExecutionInfo" should {
    "convert from AWS" in {
      val runId = "random run id"
      val workflowId = "random execution id"
      val closeStatus = CloseStatus.Completed
      val status = "CLOSE"
      val startTime = DateTime.now()
      val endTime = DateTime.now()

      val stigWorkflowExectuionInfo = new SWFWorkflowExecutionInfo()
        .withExecution(
          new SWFWorkflowExecution()
            .withRunId(runId)
            .withWorkflowId(workflowId))
        .withCloseStatus(closeStatus.asAws)
        .withExecutionStatus(status)
        .withStartTimestamp(startTime.toDate)
        .withCloseTimestamp(endTime.toDate).asStig

      stigWorkflowExectuionInfo must_== WorkflowExecutionInfo(
        WorkflowExecution(runId, workflowId),
        Some(closeStatus),
        status,
        startTime,
        Some(endTime))
    }
  }

  "WorkflowExecutionInfos" should {
    "convert from AWS" in {
      val runId = "random run id"
      val workflowId = "random execution id"
      val nextPageToken = "next page"
      val closeStatus = CloseStatus.Completed
      val status = "CLOSE"
      val startTime = DateTime.now()
      val endTime = DateTime.now()

      val stigWorkflowExectuionInfo = new SWFWorkflowExecutionInfos()
        .withExecutionInfos(
          new SWFWorkflowExecutionInfo()
            .withExecution(
              new SWFWorkflowExecution()
                .withRunId(runId)
                .withWorkflowId(workflowId))
            .withCloseStatus(closeStatus.asAws)
            .withExecutionStatus(status)
            .withStartTimestamp(startTime.toDate)
            .withCloseTimestamp(endTime.toDate))
        .withNextPageToken(nextPageToken).asStig

      stigWorkflowExectuionInfo must_== WorkflowExecutionInfos(
        Seq(
          WorkflowExecutionInfo(
            WorkflowExecution(runId, workflowId),
            Some(closeStatus),
            status,
            startTime,
            Some(endTime))),
        Some(nextPageToken))
    }
  }

  "Interval" should {
    "convert to AWS" in {
      val startDate = DateTime.now()
      val endDate = startDate.plusDays(1)

      val awsInterval = new Interval(startDate, endDate).asAws

      awsInterval must_== new ExecutionTimeFilter()
        .withOldestDate(startDate.toDate)
        .withLatestDate(endDate.toDate)
    }
  }

  "Workflow type conversion" should {
    "convert from AWS" in {
      val name = "workflow-name"
      val version = "1.0"

      val stigWorkflow = new WorkflowType()
        .withName(name)
        .withVersion(version).asStig

      stigWorkflow must_== Workflow(name, version)
    }

    "convert to AWS" in {
      val name = "workflow-name"
      val version = "1.0"

      val awsWorkflow = Workflow(name, version).asAws

      (awsWorkflow.getName must_== name) and (
        awsWorkflow.getVersion must_== version)
    }
  }

  "CloseStatus type conversion" should {
    "convert from AWS" in {
      SWFCloseStatus.values.map(_.asStig).toSet must_== CloseStatus.values.toSet
    }

    "convert to AWS" in {
      CloseStatus.values.map(_.asAws).toSet must_== SWFCloseStatus.values.toSet
    }
  }
}
