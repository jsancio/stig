package stig

import stig.serial.Reader
import stig.serial.Writer

trait DecisionHandler[Input]
    extends ((DeciderService, Array[Byte]) => Unit)
    with Reader[Input] {
  def apply(context: DeciderService, input: Array[Byte]): Unit = {
    handleDecision(context, read(input))
  }

  def handleDecision(context: DeciderService, input: Input): Unit
}
