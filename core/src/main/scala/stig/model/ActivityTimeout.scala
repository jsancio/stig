package stig.model

import org.joda.time.Seconds

final case class ActivityTimeout(
  scheduleToStart: Seconds,
  scheduleToClose: Seconds,
  startToClose: Seconds,
  heartbeat: Seconds)
