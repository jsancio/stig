package stig.model

sealed trait ScheduleActivityFailedCause {
  val name: String
}

object ScheduleActivityFailedCause {
  object ActivityCreationRateExceeded extends ScheduleActivityFailedCause {
    val name = "ACTIVITY_CREATION_RATE_EXCEEDED"
  }

  object ActivityIdAlreadyInUse extends ScheduleActivityFailedCause {
    val name = "ACTIVITY_ID_ALREADY_IN_USE"
  }

  object ActivityTypeDeprecated extends ScheduleActivityFailedCause {
    val name = "ACTIVITY_TYPE_DEPRECATED"
  }

  object ActivityTypeDoesNotExist extends ScheduleActivityFailedCause {
    val name = "ACTIVITY_TYPE_DOES_NOT_EXIST"
  }

  object DefaultHeartbeatTimeoutUndefined extends ScheduleActivityFailedCause {
    val name = "DEFAULT_HEARTBEAT_TIMEOUT_UNDEFINED"
  }

  object DefaultScheduleToCloseTimeoutUndefined extends ScheduleActivityFailedCause {
    val name = "DEFAULT_SCHEDULE_TO_CLOSE_TIMEOUT_UNDEFINED"
  }

  object DefaultScheduleToStartTimeoutUndefined extends ScheduleActivityFailedCause {
    val name = "DEFAULT_SCHEDULE_TO_START_TIMEOUT_UNDEFINED"
  }

  object DefaultStartToCloseTimeoutUndefined extends ScheduleActivityFailedCause {
    val name = "DEFAULT_START_TO_CLOSE_TIMEOUT_UNDEFINED"
  }

  object DefaultTaskListUndefined extends ScheduleActivityFailedCause {
    val name = "DEFAULT_TASK_LIST_UNDEFINED"
  }

  object OpenActivitiesLimitExceeded extends ScheduleActivityFailedCause {
    val name = "OPEN_ACTIVITIES_LIMIT_EXCEEDED"
  }

  object OperationNotPermitted extends ScheduleActivityFailedCause {
    val name = "OPERATION_NOT_PERMITTED"
  }

  private[this] val values = Map(
    (ActivityTypeDeprecated.name, ActivityTypeDeprecated),
    (ActivityTypeDoesNotExist.name, ActivityTypeDoesNotExist),
    (ActivityIdAlreadyInUse.name, ActivityIdAlreadyInUse),
    (OpenActivitiesLimitExceeded.name, OpenActivitiesLimitExceeded),
    (ActivityCreationRateExceeded.name, ActivityCreationRateExceeded),
    (DefaultScheduleToCloseTimeoutUndefined.name, DefaultScheduleToCloseTimeoutUndefined),
    (DefaultTaskListUndefined.name, DefaultTaskListUndefined),
    (DefaultScheduleToStartTimeoutUndefined.name, DefaultScheduleToStartTimeoutUndefined),
    (DefaultStartToCloseTimeoutUndefined.name, DefaultStartToCloseTimeoutUndefined),
    (DefaultHeartbeatTimeoutUndefined.name, DefaultHeartbeatTimeoutUndefined),
    (OperationNotPermitted.name, OperationNotPermitted))

  def withName(name: String): ScheduleActivityFailedCause = {
    values(name)
  }
}
