package stig.model

import org.joda.time.Seconds
import org.joda.time.Interval

final case class Activity(name: String, version: String)

final case class Workflow(name: String, version: String)

final case class TaskList(name: String)

final case class Domain(name: String)

object ChildPolicy extends Enumeration {
  type ChildPolicy = Value
  val Terminate, RequestCancel, Abandon = Value
}

final case class WorkflowTimeout(executionStartToClose: Seconds, taskStartToClose: Seconds)

final case class WorkflowCount(count: Int, truncated: Boolean)

object IntervalType extends Enumeration {
  type IntervalType = Value

  val Start, Close = Value
}

final case class IntervalFilter(
  filter: IntervalType.IntervalType,
  interval: Interval)

sealed trait WorkflowFilter
final case class ExecutionFilter(id: String) extends WorkflowFilter
final case class TagFilter(tag: String) extends WorkflowFilter
final case class TypeFilter(workflow: Workflow) extends WorkflowFilter
final case class StatusFilter(status: CloseStatus) extends WorkflowFilter
