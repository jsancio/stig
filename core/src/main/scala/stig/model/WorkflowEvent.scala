package stig.model

import org.joda.time.DateTime
import org.joda.time.Seconds

sealed trait WorkflowEvent {
  val id: Long
  val timestamp: DateTime
}

object WorkflowEvent {
  final case class WorkflowExecutionStarted(
    id: Long,
    timestamp: DateTime,
    workflow: Workflow,
    input: Array[Byte]) extends WorkflowEvent

  final case class WorkflowExecutionFailed(
    id: Long,
    timestamp: DateTime,
    reason: String,
    cause: String,
    decisionTaskCompletedEventId: Long) extends WorkflowEvent

  final case class WorkflowExecutionCompleted(
    id: Long,
    timestamp: DateTime,
    result: Array[Byte],
    decisionTaskCompletedEventId: Long) extends WorkflowEvent

  final case class DecisionTaskScheduled(
    id: Long,
    timestamp: DateTime) extends WorkflowEvent

  final case class DecisionTaskStarted(
    id: Long,
    timestamp: DateTime,
    scheduledEventId: Long) extends WorkflowEvent

  final case class DecisionTaskCompleted(
    id: Long,
    timestamp: DateTime,
    scheduledEventId: Long,
    startedEventId: Long) extends WorkflowEvent

  final case class DecisionTaskTimedOut(
    id: Long,
    timestamp: DateTime,
    timeoutType: String,
    scheduledEventId: Long,
    startedEventId: Long) extends WorkflowEvent

  final case class ActivityTaskScheduled(
    id: Long,
    timestamp: DateTime,
    activityId: Int,
    activity: Activity,
    decisionTaskCompletedEventId: Long,
    activityTimeout: ActivityTimeout,
    input: Array[Byte],
    taskList: TaskList) extends WorkflowEvent

  final case class ActivityTaskStarted(
    id: Long,
    timestamp: DateTime,
    identity: String,
    scheduledEventId: Long) extends WorkflowEvent

  final case class ActivityTaskCompleted(
    id: Long,
    timestamp: DateTime,
    scheduledEventId: Long,
    startedEventId: Long,
    result: Array[Byte]) extends WorkflowEvent

  final case class ActivityTaskFailed(
    id: Long,
    timestamp: DateTime,
    scheduledEventId: Long,
    startedEventId: Long,
    reason: String,
    details: Array[Byte]) extends WorkflowEvent

  final case class ActivityTaskTimedOut(
    id: Long,
    timestamp: DateTime,
    scheduledEventId: Long,
    startedEventId: Long,
    timeoutType: String,
    details: Option[String]) extends WorkflowEvent

  final case class ScheduleActivityTaskFailed(
    id: Long,
    timestamp: DateTime,
    activity: Activity,
    activityId: Int,
    cause: ScheduleActivityFailedCause,
    decisionTaskCompletedEventId: Long) extends WorkflowEvent

  final case class FailWorkflowExecutionFailed(
    id: Long,
    timestamp: DateTime,
    cause: String,
    decisionTaskCompletedEventId: Long) extends WorkflowEvent

  final case class TimerStarted(
    id: Long,
    timestamp: DateTime,
    timerId: Int,
    startToFireTimeout: Seconds,
    decisionTaskCompletedEventId: Long) extends WorkflowEvent

  final case class TimerFired(
    id: Long,
    timestamp: DateTime,
    timerId: Int,
    startedEventId: Long) extends WorkflowEvent

  final case class StartTimerFailed(
    id: Long,
    timestamp: DateTime,
    timerId: Int,
    cause: String,
    decisionTaskCompletedEventId: Long) extends WorkflowEvent
}
