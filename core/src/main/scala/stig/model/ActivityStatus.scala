package stig.model

import scala.annotation.switch

object ActivityStatus {
  val Scheduled = ActivityStatus("Scheduled")
  val Started = ActivityStatus("Started")
  val Completed = ActivityStatus("Completed")
  val TimedOut = ActivityStatus("TimedOut")
  val Failed = ActivityStatus("Failed")

  def fromName(value: String): ActivityStatus = {
    value match {
      case Scheduled.value => Scheduled
      case Started.value => Started
      case Completed.value => Completed
      case TimedOut.value => TimedOut
      case Failed.value => Failed
    }
  }
}

final case class ActivityStatus(value: String)
