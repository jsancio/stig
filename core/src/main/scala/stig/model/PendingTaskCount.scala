package stig.model

case class PendingTaskCount(truncated: Boolean, count: Int)
