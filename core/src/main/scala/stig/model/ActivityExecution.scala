package stig.model

import org.joda.time.DateTime

final case class ActivityExecution(
  activityId: Long,
  activity: Activity,
  activityTimeout: ActivityTimeout,
  taskList: TaskList,
  input: Array[Byte],
  decisionTaskCompletedEventId: Long,
  scheduleTime: DateTime,

  status: ActivityStatus,

  startTime: Option[DateTime],
  identity: Option[String],

  endTime: Option[DateTime],

  result: Option[Array[Byte]],

  reason: Option[String],
  details: Option[Array[Byte]], // Set by both failed and timeout

  timeoutType: Option[String])
