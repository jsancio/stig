package stig.model

import org.joda.time.Seconds

sealed trait Decision

object Decision {
  final case class ContinueAsNewWorkflow(input: Array[Byte]) extends Decision
  final case class StartChildWorkflow(id: String, workflow: Workflow, input: Array[Byte])
    extends Decision
  final case class FailWorkflow(reason: String, details: Array[Byte]) extends Decision
  final case class CompleteWorkflow(result: Array[Byte]) extends Decision
  final case class ScheduleActivityTask(
    activity: Activity,
    taskList: TaskList,
    id: Int,
    input: Array[Byte],
    timeout: ActivityTimeout) extends Decision
  final case class StartTimer(id: Int, timeout: Seconds) extends Decision
}
