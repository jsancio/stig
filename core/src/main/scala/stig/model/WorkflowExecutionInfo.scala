package stig.model

import org.joda.time.DateTime

final case class WorkflowExecutionInfo(
  execution: WorkflowExecution,
  closeStatus: Option[CloseStatus],
  executionStatus: String, // TODO: maybe we can type this better
  startTimestamp: DateTime,
  closeTimestamp: Option[DateTime])
