package stig.model

object CloseStatus {
  val Canceled = CloseStatus("Canceled")
  val Completed = CloseStatus("Completed")
  val ContinuedAsNew = CloseStatus("ContinuedAsNew")
  val Failed = CloseStatus("Failed")
  val Terminated = CloseStatus("Terminated")
  val TimedOut = CloseStatus("TimedOut")

  private[this] val valuesMap = Map[String, CloseStatus](
    (Canceled.value, Canceled),
    (Completed.value, Completed),
    (ContinuedAsNew.value, ContinuedAsNew),
    (Failed.value, Failed),
    (Terminated.value, Terminated),
    (TimedOut.value, TimedOut))

  val values: Iterable[CloseStatus] = valuesMap.values

  def fromName(value: String): CloseStatus = valuesMap(value)
}

case class CloseStatus(value: String)
