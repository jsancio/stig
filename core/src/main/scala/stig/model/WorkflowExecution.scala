package stig.model

final case class WorkflowExecution(runId: String, workflowId: String)
