package stig.model

final case class WorkflowExecutionInfos(
  executions: Seq[WorkflowExecutionInfo],
  nextPageToken: Option[String])
