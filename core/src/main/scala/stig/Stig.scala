package stig

import model.WorkflowEvent
import model.Workflow
import model.Decision
import model.Activity

object Stig {
  def makeDecisions(
    previousId: Long,
    newId: Long,
    events: Iterable[WorkflowEvent],
    deciders: Map[Workflow, Decider]): Iterable[Decision] = {

    new DecisionImpl(previousId, newId, events, deciders).makeDecisions()
  }

  def executeActivity(
    activity: Activity,
    input: Array[Byte],
    workers: Map[Activity, Worker]): Array[Byte] = {

    new ActivityImpl().executeActivity(activity, input, workers)
  }
}
