package stig

import model.Activity

final class ActivityImpl extends ActivityService {
  def executeActivity(
    activity: Activity,
    input: Array[Byte],
    workers: Map[Activity, Worker]): Array[Byte] = {

    workers.get(activity) match {
      case Some(worker) => worker(this, input)
      case None => throw new ActivityWorkerNotFoundException(activity)
    }
  }
}
