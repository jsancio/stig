package stig

import scala.collection.mutable

import com.typesafe.scalalogging.LazyLogging
import com.typesafe.scalalogging.Logger
import org.joda.time.Seconds

import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.Decision
import stig.model.ScheduleActivityFailedCause
import stig.model.TaskList
import stig.model.Workflow
import stig.model.WorkflowEvent
import stig.serial.exceptionReaderWriter
import stig.serial.Reader
import stig.serial.Writer
import stig.util.Later
import stig.util.Signal

/*
 * TODO: when we are making decision in replay mode (when isLive is false) we need to make sure
 * that the decisions it makes matches what the workflow history. This is helpful to catch
 * cases where the decider changes and it doesn't match the behaviour of the previous decider.
 */

final class DecisionImpl(
  previousId: Long,
  newId: Long,
  events: Iterable[WorkflowEvent],
  deciders: Map[Workflow, Decider])
    extends InternalDeciderContext with LazyLogging {

  private[this] var currentId = 0L
  private[this] var actionId = 0
  private[this] val state = createWorkflowEventState(events)

  val decisions = mutable.Buffer.empty[Decision]
  val taskPromises = mutable.Map.empty[Int, Signal[Array[Byte]]]
  val timerPromises = mutable.Map.empty[Int, Signal[Unit]]

  private def removeTaskSignal(id: Int): Signal[Array[Byte]] = {
    taskPromises.remove(id).get
  }

  private def removeTimerSignal(id: Int): Signal[Unit] = {
    timerPromises.remove(id).get
  }

  def isLive: Boolean = currentId > previousId && currentId < newId

  def nextId(): Int = {
    actionId += 1
    actionId
  }

  def makeDecisions(): Iterable[Decision] = {
    for (event <- events) {
      currentId = event.id
      handleEvent(event)
    }

    logger.info(s"Made the following decisions: $decisions")
    decisions
  }

  // scalastyle:off cyclomatic.complexity
  private def handleEvent(genericEvent: WorkflowEvent): Unit = genericEvent match {
    case event: WorkflowEvent.WorkflowExecutionStarted => handleWorkflowExecutionStarted(event)
    case event: WorkflowEvent.ActivityTaskCompleted => handleActivityTaskCompleted(event)
    case event: WorkflowEvent.ActivityTaskFailed => handleActivityTaskFailed(event)
    case event: WorkflowEvent.ActivityTaskTimedOut => handleActivityTaskTimedOut(event)
    case event: WorkflowEvent.TimerFired => handleTimerFired(event)
    case event: WorkflowEvent.ScheduleActivityTaskFailed => handleScheduleActivityTaskFailed(event)
    case event: WorkflowEvent.StartTimerFailed => handleStartTimerFailed(event)
    // Ignore these type of events
    case _: WorkflowEvent.WorkflowExecutionFailed =>
    case _: WorkflowEvent.WorkflowExecutionCompleted =>
    case _: WorkflowEvent.ActivityTaskScheduled =>
    case _: WorkflowEvent.ActivityTaskStarted =>
    case _: WorkflowEvent.DecisionTaskScheduled =>
    case _: WorkflowEvent.DecisionTaskStarted =>
    case _: WorkflowEvent.DecisionTaskCompleted =>
    case _: WorkflowEvent.TimerStarted =>
    case _: WorkflowEvent.DecisionTaskTimedOut =>
    // TODO: we should really handel this by making failing a workflow return a Later
    case event: WorkflowEvent.FailWorkflowExecutionFailed =>
      logger.warn(s"Not handling workflow event: $event")
  }
  // scalastyle:on cyclomatic.complexity

  private def handleWorkflowExecutionStarted(
    event: WorkflowEvent.WorkflowExecutionStarted): Unit = {

    logger.info(s"Handling workflow started event: ${event.id}")

    // Find the decider responsible for the start event
    deciders.get(event.workflow) match {
      case Some(decider) => decider(this, event.input)
      case None => throw new DeciderNotFoundException(event.workflow)
    }
  }

  private def handleActivityTaskCompleted(event: WorkflowEvent.ActivityTaskCompleted): Unit = {
    logger.info(s"Handling activity completed event: ${event.id}")

    // Handle activity task completed event
    val task = state(event.scheduledEventId).asInstanceOf[WorkflowEvent.ActivityTaskScheduled]

    // Complete the later for the Schedule task
    removeTaskSignal(task.activityId).success(event.result)
  }

  private def handleActivityTaskFailed(event: WorkflowEvent.ActivityTaskFailed): Unit = {
    logger.info(s"Handling activity failed event: ${event.id}")

    val task = state(event.scheduledEventId).asInstanceOf[WorkflowEvent.ActivityTaskScheduled]

    removeTaskSignal(task.activityId).failure(
      new ActivityFailedException(
        task.activity,
        event.reason,
        exceptionReaderWriter.read(event.details)))
  }

  private def handleActivityTaskTimedOut(event: WorkflowEvent.ActivityTaskTimedOut): Unit = {
    logger.info(s"Handling activity timedout event: ${event.id}")

    val task = state(event.scheduledEventId).asInstanceOf[WorkflowEvent.ActivityTaskScheduled]

    removeTaskSignal(task.activityId).failure(
      new ActivityTimedOutException(
        task.activity,
        event.timeoutType,
        event.details))
  }

  private def handleScheduleActivityTaskFailed(
    event: WorkflowEvent.ScheduleActivityTaskFailed): Unit = {

    logger.info(s"Handling schedule activity task failed event: ${event.id}")

    removeTaskSignal(event.activityId).failure(
      new ScheduleActivityFailedException(event.activity, event.cause))
  }

  private def handleTimerFired(event: WorkflowEvent.TimerFired): Unit = {
    logger.info(s"Handling timer fired event: ${event.id}")

    // Complete the later for the Timer task
    removeTimerSignal(event.timerId).success(())
  }

  private def handleStartTimerFailed(event: WorkflowEvent.StartTimerFailed): Unit = {
    logger.info(s"Handling start timer failed event: ${event.id}")

    // Fail the later for the Timer task
    removeTimerSignal(event.timerId).failure(new StartTimerFailedException(event.cause))
  }
}

trait InternalDeciderContext extends DeciderService { this: LazyLogging =>
  val taskPromises: mutable.Map[Int, Signal[Array[Byte]]]
  val timerPromises: mutable.Map[Int, Signal[Unit]]
  val decisions: mutable.Buffer[Decision]

  def isLive: Boolean
  def nextId(): Int

  def startTimer(timeout: Seconds): Later[Unit] = {
    val signal = Signal[Unit]()

    // Generate a predictable id
    val id = nextId()

    // If this is a replay then don't remember the decision
    if (isLive) {
      decisions += Decision.StartTimer(id, timeout)
    }

    // Remember the promise
    timerPromises += (id -> signal)

    signal.later
  }

  def scheduleActivity[Input, Output](
    activity: Activity,
    taskList: TaskList,
    timeout: ActivityTimeout,
    input: Input)(
      implicit inputWriter: Writer[Input],
      resultReader: Reader[Output]): Later[Output] = {

    val signal = Signal[Array[Byte]]()

    // Generate a predictable id
    val id = nextId()

    // If this is a replay then don't remember the decision
    if (isLive) {
      decisions += Decision.ScheduleActivityTask(
        activity,
        taskList,
        id,
        inputWriter.write(input),
        timeout)
    }

    // Remember the promise
    taskPromises += (id -> signal)

    signal.later.map(resultReader.read)
  }

  def completeWorkflow[Result](
    result: Result)(
      implicit resultWriter: Writer[Result]): Unit = {
    if (isLive) {
      decisions += Decision.CompleteWorkflow(resultWriter.write(result))
    }
  }

  def failWorkflow(reason: String, details: Throwable)(
    implicit exceptionWriter: Writer[Throwable]): Unit = {
    if (isLive) {
      decisions += Decision.FailWorkflow(reason, exceptionWriter.write(details))
    }
  }

  def startChildWorkflow[Input](
    id: String,
    workflow: Workflow,
    input: Input)(
      implicit inputWriter: Writer[Input]): Unit = {
    if (isLive) {
      decisions += Decision.StartChildWorkflow(id, workflow, inputWriter.write(input))
    }
  }

  def continueAsNewWorkflow[Input](input: Input)(implicit inputWriter: Writer[Input]): Unit = {
    if (isLive) {
      decisions += Decision.ContinueAsNewWorkflow(inputWriter.write(input))
    }
  }

  def logging: Logger = logger
}
