package stig.util

import scala.util.control.NonFatal
import scala.util.Failure
import scala.util.Success
import scala.util.Try

final class TryLater[U](val later: Later[U]) {
  def flatMap[S](f: Try[U] => TryLater[S]): TryLater[S] = {
    val signal = Signal[S]()

    later.onComplete { result =>
      try {
        f(result).later.onComplete(signal.complete(_))
      } catch {
        case NonFatal(e) => signal.failure(e)
      }
    }

    signal.later.tryLater
  }

  def map[S](f: Try[U] => Try[S]): TryLater[S] = {
    val signal = Signal[S]()

    later.onComplete { result =>
      try {
        f(result) match {
          case Failure(error) => signal.failure(error)
          case Success(success) => signal.success(success)
        }
      } catch {
        case NonFatal(error) => signal.failure(error)
      }
    }

    signal.later.tryLater
  }

  def foreach[S](f: Try[U] => S): Unit = {
    later.onComplete(f)
  }
}
