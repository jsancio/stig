package stig.util

import scala.collection.generic.CanBuildFrom
import scala.util.control.NonFatal
import scala.util.Failure
import scala.util.Success
import scala.util.Try

trait Later[T] {
  def isComplete: Boolean

  def onComplete[U](func: Try[T] => U): Unit

  def onFailure[U](pf: PartialFunction[Throwable, U]): Unit = {
    onComplete {
      case Failure(failure) =>
        pf.applyOrElse[Throwable, Any](failure, identity)
      case Success(_) => // Ignore success case
    }
  }

  def filter(pred: T => Boolean): Later[T] = {
    val signal = Signal[T]()

    onComplete {
      case f: Failure[_] => signal complete f.asInstanceOf[Failure[T]]
      case Success(v) =>
        try {
          if (pred(v)) {
            signal.success(v)
          } else {
            signal failure {
              new NoSuchElementException("Predicate is not satisfied")
            }
          }
        } catch {
          case NonFatal(e) => signal failure e
        }
    }

    signal.later
  }

  def withFilter(p: T => Boolean): Later[T] = filter(p)

  def recover[U >: T](pf: PartialFunction[Throwable, U]): Later[U] = {
    val signal = Signal[U]()

    onComplete {
      v => signal.complete(v.recover(pf))
    }

    signal.later
  }

  def recoverWith[U >: T](pf: PartialFunction[Throwable, Later[U]]): Later[U] = {
    val signal = Signal[U]()

    onComplete {
      case Failure(e) =>
        try {
          pf.applyOrElse(e, (_: Throwable) => this).onComplete(signal.complete)
        } catch {
          case NonFatal(error) => signal.failure(error)
        }

      case success => signal.complete(success)
    }

    signal.later
  }

  def flatMap[S](f: T => Later[S]): Later[S] = {
    val signal = Signal[S]()

    onComplete {
      case f: Failure[_] => signal complete f.asInstanceOf[Failure[S]]
      case Success(v) =>
        try {
          f(v) onComplete {
            case f: Failure[_] => signal complete f.asInstanceOf[Failure[S]]
            case Success(value) => signal success value
          }
        } catch {
          case NonFatal(e) => signal failure e
        }
    }

    signal.later
  }

  def foreach[U](f: T => U): Unit = {
    onComplete {
      case Success(v) => f(v)
      case _ => // nothing to do on failure
    }
  }

  def map[S](f: T => S): Later[S] = {
    val signal = Signal[S]()

    onComplete {
      case f: Failure[_] => signal complete f.asInstanceOf[Failure[S]]
      case Success(v) =>
        try {
          signal success f(v)
        } catch {
          case NonFatal(e) => signal failure e
        }
    }

    signal.later
  }

  def andThen[U](pf: PartialFunction[Try[T], U]): Later[T] = {
    val signal = Signal[T]()

    onComplete {
      case result =>
        try {
          if (pf.isDefinedAt(result)) pf(result)
        } finally {
          signal.complete(result)
        }
    }

    signal.later
  }

  def andThenWith[U](pf: PartialFunction[Try[T], Later[U]]): Later[T] = {
    val signal = Signal[T]()

    onComplete {
      case result =>
        try {
          pf.applyOrElse[Try[T], Later[_]](result, _ => this).onComplete {
            _ => signal.complete(result)
          }
        } catch {
          case NonFatal(error) => signal.complete(result)
        }
    }

    signal.later
  }

  lazy val tryLater = new TryLater(this)
}

object Later {
  def failed[T](exception: Throwable): Later[T] = {
    Signal.failed(exception).later
  }

  def successful[T](result: T): Later[T] = {
    Signal.successful(result).later
  }

  // TODO: Huge hack right now. Make more generic
  def sequence[A, M[X] <: TraversableOnce[X]](in: M[Later[A]])(implicit cbf: CanBuildFrom[M[Later[A]], A, M[A]]): Later[M[A]] = {
    in.foldLeft(successful(cbf())) { (resultLater, currentLater) =>
      for (result <- resultLater; current <- currentLater) yield (result += current)
    } map (_.result())
  }

  def traverse[A, B, M[X] <: TraversableOnce[X]](in: M[A])(fn: (A) => Later[B])(implicit cbf: CanBuildFrom[M[A], B, M[B]]): Later[M[B]] = {
    in.foldLeft(successful(cbf())) { (resultLater, value) =>
      val currentLater = fn(value)
      for (result <- resultLater; current <- currentLater) yield (result += current)
    } map (_.result())
  }
}
