package stig.util

import org.joda.time.DateTime

import stig.model.ActivityExecution

final class ActivityExecutionOrdering extends Ordering[ActivityExecution] {
  def compare(first: ActivityExecution, second: ActivityExecution): Int = {
    compare(first.scheduleTime, second.scheduleTime)
  }

  private def compare(first: DateTime, second: DateTime): Int = {
    if (first.isBefore(second)) {
      -1
    } else if (first.isAfter(second)) {
      1
    } else {
      0
    }
  }
}
