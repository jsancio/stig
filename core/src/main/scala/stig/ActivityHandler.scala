package stig

import stig.serial.Reader
import stig.serial.Writer

trait ActivityHandler[Input, Output]
    extends ((ActivityService, Array[Byte]) => Array[Byte])
    with Reader[Input] with Writer[Output] {

  def apply(context: ActivityService, input: Array[Byte]): Array[Byte] = {
    write(handleActivity(context, read(input)))
  }

  def handleActivity(context: ActivityService, input: Input): Output
}
