package stig

import org.joda.time.Days
import org.joda.time.Interval

import stig.model.Activity
import stig.model.ChildPolicy
import stig.model.CloseStatus
import stig.model.Domain
import stig.model.IntervalFilter
import stig.model.PendingTaskCount
import stig.model.TaskList
import stig.model.Workflow
import stig.model.WorkflowCount
import stig.model.WorkflowEvent
import stig.model.WorkflowExecution
import stig.model.WorkflowExecutionInfos
import stig.model.WorkflowFilter
import stig.model.WorkflowTimeout
import stig.serial.Writer

trait ExternalWorkflowClient {
  // scalastyle:ignore parameter.number
  def startWorkflow[Input](
    domain: Domain,
    workflow: Workflow,
    input: Input,
    tags: Seq[String],
    taskList: TaskList,
    timeout: WorkflowTimeout,
    childPolicy: ChildPolicy.ChildPolicy,
    workflowId: String)(implicit inputWriter: Writer[Input]): String

  def registerDomain(domain: Domain, description: String, retention: Days): Unit
  def registerWorkflow(domain: Domain, workflow: Workflow, description: String): Unit
  def registerActivity(domain: Domain, activity: Activity, description: String): Unit

  // Reporting methods
  def countClosedWorkflows(
    domain: Domain,
    intervalFilter: IntervalFilter,
    workflowFilter: Option[WorkflowFilter]): WorkflowCount

  def listOpenWorkflow(
    domain: Domain,
    startTimeFilter: Interval,
    maximumPageSize: Int,
    workflowFilter: Option[WorkflowFilter], // TODO: Don't support closed status filter
    nextPageToken: Option[String]): WorkflowExecutionInfos

  def listClosedWorkflow(
    domain: Domain,
    intervalFilter: IntervalFilter,
    maximumPageSize: Int,
    workflowFilter: Option[WorkflowFilter],
    nextPageToken: Option[String]): WorkflowExecutionInfos

  def getWorkflowHistory(domain: Domain, execution: WorkflowExecution): Seq[WorkflowEvent]

  def countPendingActivityTasks(domain: Domain, taskList: TaskList): PendingTaskCount
  def countPendingDecisionTasks(domain: Domain, taskList: TaskList): PendingTaskCount
}
