package stig

package object serial {
  implicit val stringReader = new StringReader {}
  implicit val stringWriter = new StringWriter {}
  implicit val byteArrayReader = new ByteArrayReader {}
  implicit val byteArrayWriter = new ByteArrayWriter {}
  val exceptionReaderWriter = new ExceptionReaderWriter
}
