package stig.serial

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.nio.charset.Charset

import scala.util.control.NonFatal

trait Reader[Value] {
  def read(input: Array[Byte]): Value
}

trait Writer[Value] {
  def write(output: Value): Array[Byte]
}

trait StringReader extends Reader[String] {
  def read(input: Array[Byte]): String = new String(input, Charset.forName("UTF-8"))
}

trait StringWriter extends Writer[String] {
  def write(output: String): Array[Byte] = output.getBytes(Charset.forName("UTF-8"))
}

trait ByteArrayReader extends Reader[Array[Byte]] {
  def read(input: Array[Byte]): Array[Byte] = input
}

trait ByteArrayWriter extends Writer[Array[Byte]] {
  def write(output: Array[Byte]): Array[Byte] = output
}

final class ExceptionReaderWriter extends Reader[Throwable] with Writer[Throwable] {
  def read(bytes: Array[Byte]): Throwable = {
    // TODO: make this code nicer. Remove the try...finally

    val deserializer = new ObjectInputStream(
      new ByteArrayInputStream(bytes))

    try {
      deserializer.readObject().asInstanceOf[Throwable]
    } finally {
      deserializer.close()
    }
  }

  def write(output: Throwable): Array[Byte] = {
    try {
      writeException(output)
    } catch {
      case NonFatal(exception) =>
        // Couldn't serialize the given exception lets serialize the new exception
        writeException(exception)
    }
  }

  private def writeException(exception: Throwable): Array[Byte] = {
    // TODO: make this code nicer. Remove the try...finally

    val buffer = new ByteArrayOutputStream()
    val serializer = new ObjectOutputStream(buffer)

    try {
      serializer.writeObject(exception)
    } finally {
      buffer.close()
      serializer.close()
    }

    // We need to do this here to guarantee that the serializer has been flushed
    buffer.toByteArray
  }
}
