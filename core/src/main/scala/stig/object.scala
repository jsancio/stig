import stig.model.ActivityExecution
import stig.model.ActivityStatus
import stig.model.WorkflowEvent
import stig.model.WorkflowEvent.ActivityTaskCompleted
import stig.model.WorkflowEvent.ActivityTaskFailed
import stig.model.WorkflowEvent.ActivityTaskScheduled
import stig.model.WorkflowEvent.ActivityTaskStarted
import stig.model.WorkflowEvent.ActivityTaskTimedOut
import stig.util.ActivityExecutionOrdering

package object stig {
  type Decider = (DeciderService, Array[Byte]) => Unit

  type Worker = (ActivityService, Array[Byte]) => Array[Byte]

  def createWorkflowEventState(events: Iterable[WorkflowEvent]): Map[Long, WorkflowEvent] = {
    events.foldLeft(Map[Long, WorkflowEvent]()) {
      (state, event) => state + (event.id -> event)
    }
  }

  def constructActivityExecution(events: Seq[WorkflowEvent]): Seq[ActivityExecution] = {
    val eventLookup = createWorkflowEventState(events)

    val activities = events.foldLeft(Map.empty[Int, ActivityExecution]) { (state, current) =>
      current match {
        case event: ActivityTaskScheduled =>
          state + ((event.activityId, createActivityExecution(event)))

        case event: ActivityTaskStarted =>
          val activityId = findActivityId(eventLookup, event.scheduledEventId)
          state + ((activityId, updateActivityExecution(state(activityId), event)))

        case event: ActivityTaskCompleted =>
          val activityId = findActivityId(eventLookup, event.scheduledEventId)
          state + ((activityId, updateActivityExecution(state(activityId), event)))

        case event: ActivityTaskFailed =>
          val activityId = findActivityId(eventLookup, event.scheduledEventId)
          state + ((activityId, updateActivityExecution(state(activityId), event)))

        case event: ActivityTaskTimedOut =>
          val activityId = findActivityId(eventLookup, event.scheduledEventId)
          state + ((activityId, updateActivityExecution(state(activityId), event)))

        case _ =>
          state
      }
    }

    activities.values.toSeq.sorted(new ActivityExecutionOrdering())
  }

  private def createActivityExecution(event: ActivityTaskScheduled): ActivityExecution = {
    ActivityExecution(
      activityId = event.activityId,
      activity = event.activity,
      activityTimeout = event.activityTimeout,
      taskList = event.taskList,
      input = event.input,
      decisionTaskCompletedEventId = event.decisionTaskCompletedEventId,
      scheduleTime = event.timestamp,
      status = ActivityStatus.Scheduled,
      startTime = None,
      identity = None,
      endTime = None,
      result = None,
      reason = None,
      details = None,
      timeoutType = None)
  }

  private def updateActivityExecution(
    activityExecution: ActivityExecution,
    event: ActivityTaskStarted): ActivityExecution = {

    activityExecution.copy(
      status = ActivityStatus.Started,
      startTime = Some(event.timestamp),
      identity = Some(event.identity))
  }

  private def updateActivityExecution(
    activityExecution: ActivityExecution,
    event: ActivityTaskCompleted): ActivityExecution = {

    activityExecution.copy(
      status = ActivityStatus.Completed,
      endTime = Some(event.timestamp),
      result = Some(event.result))
  }

  private def updateActivityExecution(
    activityExecution: ActivityExecution,
    event: ActivityTaskFailed): ActivityExecution = {

    activityExecution.copy(
      status = ActivityStatus.Failed,
      endTime = Some(event.timestamp),
      reason = Some(event.reason),
      details = Some(event.details))
  }

  private def updateActivityExecution(
    activityExecution: ActivityExecution,
    event: ActivityTaskTimedOut): ActivityExecution = {

    activityExecution.copy(
      status = ActivityStatus.TimedOut,
      endTime = Some(event.timestamp),
      timeoutType = Some(event.timeoutType),
      details = event.details.map(_.getBytes("UTF-8")))
  }

  private def findActivityId(eventLookup: Map[Long, WorkflowEvent], eventId: Long): Int = {
    eventLookup(eventId)
      .asInstanceOf[WorkflowEvent.ActivityTaskScheduled]
      .activityId
  }
}
