package stig

import com.typesafe.scalalogging.Logger
import org.joda.time.Seconds

import model.Activity
import model.ActivityTimeout
import model.TaskList
import model.Workflow
import serial.Reader
import serial.Writer
import util.Later

trait DeciderService {
  def startTimer(timeout: Seconds): Later[Unit]

  def scheduleActivity[Input, Output](
    activity: Activity,
    taskList: TaskList,
    timeout: ActivityTimeout,
    input: Input)(
      implicit inputWriter: Writer[Input],
      resultRead: Reader[Output]): Later[Output]

  def completeWorkflow[Result](result: Result)(implicit resultWriter: Writer[Result]): Unit

  def failWorkflow(reason: String, details: Throwable)(
    implicit exceptionWriter: Writer[Throwable]): Unit

  def startChildWorkflow[Input](
    id: String,
    workflow: Workflow,
    input: Input)(
      implicit inputWriter: Writer[Input]): Unit

  def continueAsNewWorkflow[Input](input: Input)(implicit inputWriter: Writer[Input]): Unit

  def logging: Logger
}
