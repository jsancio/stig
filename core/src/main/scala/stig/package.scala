package stig

import stig.model.Activity
import stig.model.ScheduleActivityFailedCause
import stig.model.Workflow

final class ActivityFailedException(
  val activity: Activity,
  val reason: String,
  val cause: Throwable) extends Exception(s"$activity: $reason", cause)

final class ActivityTimedOutException(
  val activity: Activity,
  val timeoutType: String,
  val details: Option[String]) extends Exception(s"$activity $timeoutType - $details")

final class ScheduleActivityFailedException(
  val activity: Activity,
  val cause: ScheduleActivityFailedCause) extends Exception(s"$activity failed: $cause")

final class ActivityWorkerNotFoundException(
  val activity: Activity) extends Exception(activity.toString)

final class DeciderNotFoundException(
  val workflow: Workflow) extends Exception(workflow.toString)

final class StartTimerFailedException(
  val cause: String) extends Exception(cause)

final class WorkflowAlreadyStartedException(
  val requestId: String,
  cause: Throwable) extends Exception(requestId, cause)

final class ThrottlingException(
  message: String,
  cause: Throwable) extends Exception(message, cause)
