package stig

import java.nio.charset.Charset

import org.joda.time.{ DateTime, Seconds }
import org.specs2.mutable

import stig.model.Activity
import stig.model.ActivityTimeout
import stig.model.Decision
import stig.model.ScheduleActivityFailedCause
import stig.model.TaskList
import stig.model.Workflow
import stig.model.WorkflowEvent
import stig.serial.exceptionReaderWriter
import stig.serial.stringReader
import stig.serial.stringWriter

final class DeciderSpec extends mutable.Specification {
  val workflow = Workflow("test-workflow", "1.0")
  val activity = Activity("test-activity", "1.0")
  val workflowInput = "test input".getBytes("UTF-8")
  val taskList = TaskList("test-tasklist")
  implicit val exceptionRerWer = exceptionReaderWriter

  "Decider" should {
    "complete a workflow" in {
      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.completeWorkflow(input)
      }

      val decisions = Stig.makeDecisions(
        0,
        3,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== workflowInput)
    }

    "fail a workflow" in {
      val failingReason = "failing reason"
      var exception: Throwable = null
      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        exception = new Exception(new String(input, Charset.forName("UTF-8")))
        context.failWorkflow(failingReason, exception)
      }

      val decisions = Stig.makeDecisions(
        0,
        3,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2)),
        Map(workflow -> decider))

      val serialException = exceptionReaderWriter.write(exception)

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.FailWorkflow].reason must_== failingReason) and (
          decisions.head.asInstanceOf[Decision.FailWorkflow].details must_== serialException) and (
            exceptionReaderWriter.read(serialException).toString must_== exception.toString)
    }

    "schedule a task" in {
      val timeout = ActivityTimeout(
        Seconds.seconds(1),
        Seconds.seconds(2),
        Seconds.seconds(3),
        Seconds.seconds(4))
      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.scheduleActivity[Array[Byte], Array[Byte]](activity, taskList, timeout, input)
      }

      val decisions = Stig.makeDecisions(
        0,
        3,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head must_== Decision.ScheduleActivityTask(
          activity,
          taskList,
          1,
          workflowInput,
          timeout))
    }

    "complete workflow after successful task" in {
      val activityResult = "test activity result".getBytes("UTF-8")
      val timeout = ActivityTimeout(
        Seconds.seconds(1),
        Seconds.seconds(2),
        Seconds.seconds(3),
        Seconds.seconds(4))

      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.scheduleActivity[Array[Byte], Array[Byte]](
          activity, taskList, timeout, input).foreach {

            result => context.completeWorkflow(result)
          }
      }

      val decisions = Stig.makeDecisions(
        3,
        9,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2),
          WorkflowEvent.DecisionTaskCompleted(4, DateTime.now(), 2, 3),
          WorkflowEvent.ActivityTaskScheduled(
            5,
            DateTime.now(),
            1,
            activity,
            4,
            timeout,
            workflowInput,
            taskList),
          WorkflowEvent.ActivityTaskStarted(6, DateTime.now(), "id", 5),
          WorkflowEvent.ActivityTaskCompleted(7, DateTime.now(), 5, 6, activityResult),
          WorkflowEvent.DecisionTaskScheduled(8, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(9, DateTime.now(), 8)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== activityResult)
    }

    "complete workflow after failed task" in {
      val failureReason = "failure reason"
      val failureCause = new Exception("failure cause")
      val timeout = ActivityTimeout(
        Seconds.seconds(1),
        Seconds.seconds(2),
        Seconds.seconds(3),
        Seconds.seconds(4))

      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.scheduleActivity[Array[Byte], Array[Byte]](
          activity, taskList, timeout, input) onFailure {

            case failure: ActivityFailedException =>
              context.completeWorkflow((failure.reason + failure.cause).getBytes("UTF-8"))
          }
      }

      val serialError = exceptionReaderWriter.write(failureCause)

      val decisions = Stig.makeDecisions(
        3,
        9,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2),
          WorkflowEvent.DecisionTaskCompleted(4, DateTime.now(), 2, 3),
          WorkflowEvent.ActivityTaskScheduled(
            5,
            DateTime.now(),
            1,
            activity,
            4,
            timeout,
            workflowInput,
            taskList),
          WorkflowEvent.ActivityTaskStarted(6, DateTime.now(), "id", 5),
          WorkflowEvent.ActivityTaskFailed(7, DateTime.now(), 5, 6, failureReason, serialError),
          WorkflowEvent.DecisionTaskScheduled(8, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(9, DateTime.now(), 8)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== (
          (failureReason + failureCause).getBytes("UTF-8")))
    }

    "complete workflow after timedout task" in {
      val timeoutType = "test timeout type"
      val details = Some("test details")
      val timeout = ActivityTimeout(
        Seconds.seconds(1),
        Seconds.seconds(2),
        Seconds.seconds(3),
        Seconds.seconds(4))

      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.scheduleActivity[Array[Byte], Array[Byte]](
          activity, taskList, timeout, input) onFailure {

            case failure: ActivityTimedOutException =>
              context.completeWorkflow(failure.timeoutType + failure.details)
          }
      }

      val decisions = Stig.makeDecisions(
        3,
        9,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2),
          WorkflowEvent.DecisionTaskCompleted(4, DateTime.now(), 2, 3),
          WorkflowEvent.ActivityTaskScheduled(
            5,
            DateTime.now(),
            1,
            activity,
            4,
            timeout,
            workflowInput,
            taskList),
          WorkflowEvent.ActivityTaskStarted(6, DateTime.now(), "id", 5),
          WorkflowEvent.ActivityTaskTimedOut(7, DateTime.now(), 5, 6, timeoutType, details),
          WorkflowEvent.DecisionTaskScheduled(8, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(9, DateTime.now(), 8)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== (
          (timeoutType + details).getBytes("UTF-8")))
    }

    "complete workflow after schedule task failed" in {
      val cause = ScheduleActivityFailedCause.ActivityTypeDeprecated
      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        val timeout = ActivityTimeout(
          Seconds.seconds(1),
          Seconds.seconds(2),
          Seconds.seconds(3),
          Seconds.seconds(4))
        context.scheduleActivity[Array[Byte], Array[Byte]](
          activity, taskList, timeout, input) onFailure {

            case failure: ScheduleActivityFailedException =>
              context.completeWorkflow(failure.cause.toString.getBytes("UTF-8"))
          }
      }

      val decisions = Stig.makeDecisions(
        3,
        9,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2),
          WorkflowEvent.DecisionTaskCompleted(4, DateTime.now(), 2, 3),
          WorkflowEvent.ScheduleActivityTaskFailed(5, DateTime.now(), activity, 1, cause, 4),
          WorkflowEvent.DecisionTaskScheduled(6, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(7, DateTime.now(), 6)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== (
          cause.toString.getBytes("UTF-8")))
    }

    "schedule timer" in {
      val seconds = Seconds.seconds(3)

      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.startTimer(seconds)
      }

      val decisions = Stig.makeDecisions(
        0,
        3,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (decisions.head must_== Decision.StartTimer(1, seconds))
    }

    "complete workflow after timer" in {
      val seconds = Seconds.seconds(3)

      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.startTimer(seconds).foreach { _ =>
          context.completeWorkflow(seconds.toString)
        }
      }

      val decisions = Stig.makeDecisions(
        3,
        8,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2),
          WorkflowEvent.DecisionTaskCompleted(4, DateTime.now(), 2, 3),
          WorkflowEvent.TimerStarted(5, DateTime.now(), 1, seconds, 4),
          WorkflowEvent.TimerFired(6, DateTime.now(), 1, 5),
          WorkflowEvent.DecisionTaskScheduled(7, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(8, DateTime.now(), 7)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== (
          seconds.toString.getBytes("UTF-8")))
    }

    "complete workflow after start timer failed" in {
      val seconds = Seconds.seconds(3)
      val cause = "test cause"

      def decider(context: DeciderService, input: Array[Byte]): Unit = {
        context.startTimer(seconds).onFailure {
          case failure: StartTimerFailedException =>
            context.completeWorkflow(failure.cause)
        }
      }

      val decisions = Stig.makeDecisions(
        3,
        8,
        Seq(
          WorkflowEvent.WorkflowExecutionStarted(1, DateTime.now(), workflow, workflowInput),
          WorkflowEvent.DecisionTaskScheduled(2, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(3, DateTime.now(), 2),
          WorkflowEvent.DecisionTaskCompleted(4, DateTime.now(), 2, 3),
          WorkflowEvent.StartTimerFailed(5, DateTime.now(), 1, cause, 4),
          WorkflowEvent.DecisionTaskScheduled(6, DateTime.now()),
          WorkflowEvent.DecisionTaskStarted(7, DateTime.now(), 6)),
        Map(workflow -> decider))

      (decisions.size must_== 1) and (
        decisions.head.asInstanceOf[Decision.CompleteWorkflow].result must_== (
          cause.getBytes("UTF-8")))
    }
  }
}
