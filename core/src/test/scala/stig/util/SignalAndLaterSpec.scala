package stig.util

import java.lang.Throwable

import scala.util.{ Try, Success, Failure }

import org.specs2.mutable

case class SomeCoolThrowable(m: String) extends Throwable(m)
case object InvalidTestPath extends Throwable

final class SignalAndLaterSpec extends mutable.Specification {
  private[this] val result = "special value"

  "Signal" should {
    "fire onComplete if it is registered before completing" in {
      val signal = Signal[String]()

      var success = false
      signal.later.onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }
      signal.complete(Success(result))

      success must_== true
    }

    "be able to registere multiple onCompletes" in {
      val signal = Signal[String]()

      var firstSuccess = false
      signal.later.onComplete {
        case Success(v) => firstSuccess = v == result
        case Failure(f) => throw InvalidTestPath
      }

      var secondSuccess = false
      signal.later.onComplete {
        case Success(v) => secondSuccess = v == result
        case Failure(f) => throw InvalidTestPath
      }

      signal.complete(Success(result))

      (firstSuccess must_== true) and (secondSuccess must_== true)
    }

    "fire if complete happens before onComplete" in {
      val signal = Signal[String]()

      var success = false
      signal.complete(Success(result))
      signal.later.onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }

      success must_== true
    }

    "should not change a value if it is not completed" in {
      val signal = Signal[String]()

      var success = false
      signal.later.onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }

      success must_== false
    }

    "complete the signal as a failure" in {
      val signal = Signal[String]()

      var failure = false
      signal.later.onComplete {
        case Success(v) => throw InvalidTestPath
        case Failure(f) => failure = f.isInstanceOf[SomeCoolThrowable]
      }

      signal.failure(SomeCoolThrowable("failed"))

      failure must_== true
    }
  }

  "Later" should {
    "be able to recover from a failure with a Later" in {
      val signal = Signal[String]()

      var success = false
      signal.later.recoverWith {
        case SomeCoolThrowable(msg) =>
          Later.successful(msg)
      } onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }

      signal.failure(SomeCoolThrowable(result))

      success must_== true
    }

    "be able to recover from a failure with a value" in {
      val signal = Signal[String]()

      var success = false
      signal.later.recover {
        case SomeCoolThrowable(msg) => msg
      } onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }

      signal.failure(SomeCoolThrowable(result))

      success must_== true
    }

    "only trigger recover when we get a failure" in {
      val signal = Signal[String]()

      var success = false
      signal.later.recover {
        case SomeCoolThrowable(msg) => msg + msg
      } onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }

      signal.complete(Success(result))

      success must_== true
    }

    "apply a filter predicate to successes" in {
      val signal = Signal[String]()

      var success = false
      signal.later.filter(_ == result) onComplete {
        case Success(v) => success = v == result
        case Failure(f) => throw InvalidTestPath
      }

      signal.complete(Success(result))

      success must_== true
    }

    "return a failure when the filter predicate fails" in {
      val signal = Signal[String]()

      var success = false
      signal.later.filter(_ != result) onComplete {
        case Success(_) => throw InvalidTestPath
        case Failure(f) => success = true
      }

      signal.complete(Success(result))

      success must_== true
    }

    "correctly apply map to a value within a Later" in {
      val signal = Signal[String]()

      var success = false
      signal.later.map(value => value + value) onComplete {
        case Success(v) => success = v == result + result
        case Failure(f) => throw InvalidTestPath
      }

      signal.complete(Success(result))

      success must_== true
    }

    "apply foreach a function to a successful Later" in {
      val signal = Signal[String]()

      var success = false
      signal.later.foreach(value => success = value == result)

      signal.complete(Success(result))

      success must_== true
    }

    "have flatMap return a new Later" in {
      val signal = Signal[String]()

      var success = false
      signal.later.flatMap(result => Later.successful(result + result)) onComplete {
        case Success(v) => success = v == result + result
        case Failure(f) => throw InvalidTestPath
      }

      signal.complete(Success(result))

      success must_== true
    }

    "call onFailure when failed" in {
      val signal = Signal[String]()

      var failure = false
      signal.later.onFailure {
        case SomeCoolThrowable(msg) => failure = msg == result
      }

      signal.failure(SomeCoolThrowable(result))

      failure must_== true
    }

    "call andThen after a success" in {
      val signal = Signal[String]()

      var counter = 0
      val second = signal.later.andThen {
        case Success(v) => if (v == result) counter += 1
        case Failure(f) => throw InvalidTestPath
      }

      // check that the andThen hasn't executed yet
      counter must_== 0

      second onComplete {
        case Success(v) => if (v == result) counter += 1
        case Failure(f) => throw InvalidTestPath
      }

      // check that the onComplete hasn't executed yet
      counter must_== 0

      signal.success(result)

      counter must_== 2
    }

    "call andThen after a failure" in {
      val signal = Signal[String]()

      var counter = 0
      val second = signal.later.andThen {
        case Success(v) => throw InvalidTestPath
        case Failure(SomeCoolThrowable(msg)) => if (msg == result) counter += 1
      }

      // check that the andThen hasn't executed yet
      counter must_== 0

      second onComplete {
        case Success(v) => throw InvalidTestPath
        case Failure(SomeCoolThrowable(msg)) => if (msg == result) counter += 1
        case Failure(f) => throw InvalidTestPath
      }

      // check that the onComplete hasn't executed yet
      counter must_== 0

      signal.failure(SomeCoolThrowable(result))

      counter must_== 2
    }

    "call andThenWith after a success" in {
      val signal = Signal[String]()
      val internalSignal = Signal[String]()

      var counter = 0
      val second = signal.later.andThenWith {
        case Success(v) =>
          if (v == result) counter += 1
          internalSignal.later

        case Failure(f) => throw InvalidTestPath
      }

      // check that the andThenWith hasn't executed yet
      counter must_== 0

      second.onComplete {
        case Success(v) => if (v == result) counter += 1
        case Failure(f) => throw InvalidTestPath
      }

      // check that the onComplete hasn't executed yet
      counter must_== 0

      signal.success(result)

      counter must_== 1

      internalSignal.success("")

      counter must_== 2
    }

    "call andTheWith after a failure" in {
      val signal = Signal[String]()
      val internalSignal = Signal[String]()

      var counter = 0
      val second = signal.later.andThenWith {
        case Success(v) => throw InvalidTestPath

        case Failure(SomeCoolThrowable(f)) =>
          if (f == result) counter += 1
          internalSignal.later
      }

      // check that the andThenWith hasn't executed yet
      counter must_== 0

      second.onComplete {
        case Success(v) => throw InvalidTestPath
        case Failure(SomeCoolThrowable(msg)) => if (msg == result) counter += 1
        case Failure(f) => throw InvalidTestPath
      }

      // check that the onComplete hasn't executed yet
      counter must_== 0

      signal.failure(SomeCoolThrowable(result))

      counter must_== 1

      internalSignal.success("")

      counter must_== 2
    }
  }
}
