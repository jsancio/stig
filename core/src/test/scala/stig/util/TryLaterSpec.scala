package stig.util

import org.specs2.Specification
import org.specs2.specification.Grouped

final class TryLaterSpec extends Specification with Grouped {
  def is = s2"""

  This specification checks TryLater functionality

  TryLater should
    apply flatMap on failure                      ${g1.e1}
    apply flatMap on success                      ${g1.e2}
    apply map on failure                          ${g1.e3}
    apply map on success                          ${g1.e4}
    apply foreach on failure                      ${g1.e5}
    apply foreach on success                      ${g1.e6}
                                                   """
}
