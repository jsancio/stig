package stig

import org.specs2.Specification
import org.specs2.specification.Grouped

final class ObjectSpec extends Specification with Grouped {
  def is = s2"""

  This specification tests the creation of ActivityExecution from WorkflowEvents

  constructActivityExecution should
    mark the activity as scheduled       ${g1.e1}
    mark the activity as started         ${g1.e2}
    mark the activity as completed       ${g1.e3}
    mark the activity as timedout        ${g1.e4}
    mark the activity as failed          ${g1.e5}

    mark a collections of activity       ${g1.e6}
                                         """
}
