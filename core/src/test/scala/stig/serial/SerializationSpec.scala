package stig.serial

import java.io.FileInputStream

import org.specs2.mutable

final class SerializationSpec extends mutable.Specification {
  "Exception reader and writer" should {
    "serialize and deserialize exception" in {
      val expected = new Exception("error message", new Exception("inner message"))

      val actual = exceptionReaderWriter.read(exceptionReaderWriter.write(expected))

      (actual.getMessage must_== expected.getMessage) and (
        actual.getCause.getMessage must_== expected.getCause.getMessage) and (
          actual.getCause.getStackTrace must_== expected.getCause.getStackTrace) and (
            actual.getStackTrace must_== expected.getStackTrace) and (
              actual.getSuppressed must_== expected.getSuppressed)
    }

    "should be smart about serializing exceptions that are not serializable" in {
      final class NotSerializable(val stream: FileInputStream) extends Exception

      val exception = new NotSerializable(new FileInputStream("project/plugins.sbt"))

      exceptionReaderWriter.read(
        exceptionReaderWriter.write(exception)) must not(throwA[Throwable])
    }
  }
}
