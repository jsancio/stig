import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._

name := "stig-core"

version := "0.1"

scalaVersion := "2.11.4"

packageArchetype.java_application

scalacOptions ++= Seq("-deprecation")

libraryDependencies ++= Seq(
  "io.dropwizard.metrics" % "metrics-core" % "3.1.0",
  "commons-codec" % "commons-codec" % "1.9",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "org.slf4j" % "jcl-over-slf4j" % "1.7.7",
  "org.joda" % "joda-convert" % "1.7",
  "joda-time" % "joda-time" % "2.5",
  "org.specs2" %% "specs2" % "2.4.9" % "test")

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"

scalariformSettings

