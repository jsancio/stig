lazy val root = (project.in(file(".")).
  aggregate(aws, core, mesos))

lazy val core = project

lazy val aws = project.dependsOn(core)

lazy val mesos = project.dependsOn(core)
